# Pocket Moodle

This is Pocket Moodle, an Android app to access your brand new Moodle!

![](pics/main_dark.png)

With an intuitive and easy to use interface, it allows teachers and students to perform their tasks faster than ever!

All your data is stored in [Firebase](https://firebase.google.com/), which is a fast, secure and private service offered by Google.

## Features

### Timetable

See all your subjects in one place! Say goodbye to those old paper sheets that would get lost or outdated.

![](pics/timetable_dark.png)

### Chats

Keep real time conversations with other students and teachers!

When email feels a bit too cumbersome but sharing your phone number is a no go, this is the easiest way to communicate.

![](pics/chats_dark.png)

![](pics/chat_dark.png)

### Subjects

See all your subjects at a glance. Teachers can hide and unhide them as the course advances.

![](pics/subjects_dark.png)

![](pics/topics_dark.png)

<!-- ### Tasks and exams

![Missing picture](pics/task_dark.png)

Students can keep track of upcoming tasks and exams, and teachers can see how many are pending ¿for correction?. -->