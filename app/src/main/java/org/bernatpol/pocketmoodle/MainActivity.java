package org.bernatpol.pocketmoodle;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.preference.PreferenceManager;

import org.bernatpol.pocketmoodle.subject.SubjectsViewModel;
import org.bernatpol.pocketmoodle.subject.SubjectsViewModelFactory;

public class MainActivity extends AppCompatActivity {
    private AppBarConfiguration mAppBarConfiguration;

    private SubjectsViewModel subjectsViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean student = sharedPreferences.getBoolean("student", true);

        MainActivity.this.loadNavigationDrawerUserInfo(sharedPreferences, student);

        this.subjectsViewModel = new ViewModelProvider(
                this,
                new SubjectsViewModelFactory(student)
        ).get(SubjectsViewModel.class);
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    private void loadNavigationDrawerUserInfo(SharedPreferences sharedPreferences, boolean student) {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        String userFullName = sharedPreferences.getString("userFullName", "Unknown");
        String userPicture = sharedPreferences.getString("userPicture", null);

        DrawerLayout drawerLayout = this.findViewById(R.id.drawer_layout);

        NavigationView navigationView = this.findViewById(R.id.nav_view);

        if (student) {
            navigationView.inflateMenu(R.menu.activity_main_drawer_student);
            this.mAppBarConfiguration = this.studentAppBarConfiguration(drawerLayout);
        } else {
            navigationView.inflateMenu(R.menu.activity_main_drawer_teacher);
            this.mAppBarConfiguration = this.teacherAppBarConfiguration(drawerLayout);
        }

        View navigationHeader = navigationView.getHeaderView(0);

        ((TextView) navigationHeader.findViewById(R.id.full_name)).setText(userFullName);
        ((TextView) navigationHeader.findViewById(R.id.email)).setText(firebaseUser.getEmail());

        ImageView picture = navigationHeader.findViewById(R.id.picture);

        navigationHeader.findViewById(R.id.logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Resources resources = getResources();

                new AlertDialog.Builder(MainActivity.this)
                        .setTitle(resources.getString(R.string.are_you_sure))
                        .setMessage(resources.getString(R.string.log_in_again))
                        .setPositiveButton(resources.getString(R.string.im_sure), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                FirebaseAuth.getInstance().signOut();

                                MainActivity.this.finish();

                                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton(resources.getString(R.string.go_back), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .create().show();
            }
        });

        Glide.with(MainActivity.this)
                .load(userPicture)
                .apply(RequestOptions.circleCropTransform())
                .into(picture)
        ;

        NavController navController = Navigation.findNavController(MainActivity.this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(MainActivity.this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    private AppBarConfiguration studentAppBarConfiguration(DrawerLayout drawerLayout) {
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        return new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_subjects_list, R.id.nav_chats, R.id.nav_timetable , R.id.nav_calendar)
                .setDrawerLayout(drawerLayout)
                .build()
        ;
    }

    private AppBarConfiguration teacherAppBarConfiguration(DrawerLayout drawerLayout) {
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        return new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_subjects_list, /*R.id.nav_attendance,*/ R.id.nav_chats, R.id.nav_timetable , R.id.nav_calendar)
                .setDrawerLayout(drawerLayout)
                .build()
        ;
    }
}
