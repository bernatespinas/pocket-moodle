package org.bernatpol.pocketmoodle.home;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.bernatpol.pocketmoodle.chat.Chat;
import org.bernatpol.pocketmoodle.subject.Subject;
import org.bernatpol.pocketmoodle.subject.SubjectsViewModel;
import org.bernatpol.pocketmoodle.task.Task;
import org.bernatpol.pocketmoodle.topic.Topic;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

class HomeViewModel extends ViewModel {
    // An int[] that has to contain two values in this order:
    // 1) The amount of chats with unread messages.
    // 2) The total amount of unread messages.
    private MutableLiveData<int[]> unreadChatsAndMessagesCount;

    private MutableLiveData<Optional<SubjectCardInfo>> currentSubject;
    private MutableLiveData<Optional<SubjectCardInfo>> nextSubject;

    private MutableLiveData<HashMap<String, ArrayList<Task>>> pendingTasks;
    private MutableLiveData<HashMap<String, ArrayList<Task>>> pendingExams;

    private MutableLiveData<Integer> teacherPendingTasks;
    private MutableLiveData<Integer> teacherPendingExams;
    private MutableLiveData<Optional<String[]>> teacherAttendanceStatus;

    HomeViewModel(String userRoleNode, SubjectsViewModel subjectsViewModel) {
        // ###
        // ### Get unread messages
        // ###
        this.unreadChatsAndMessagesCount = new MutableLiveData<>();

        String userUid = FirebaseAuth.getInstance().getCurrentUser().getUid();

        FirebaseDatabase.getInstance()
                .getReference("2wiam/" + userRoleNode + "/" + userUid + "/chats/")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        int unreadChats = 0;
                        int unreadMessages = 0;

                        //Mira los chats que hay sin leer y de cuantas personas son.
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            Chat chat = child.getValue(Chat.class);
                            if (chat.getUnreadMessages() > 0) {
                                unreadChats += 1;
                                unreadMessages += chat.getUnreadMessages();
                            }
                        }

                        HomeViewModel.this.unreadChatsAndMessagesCount.setValue(new int[]{unreadChats, unreadMessages});
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                })
        ;

        // ###
        // ### Initialize current and next Subjects' LiveData.
        // ### They will be fetched when SubjectsViewModel is ready.
        // ###
        this.currentSubject = new MutableLiveData<>();
        this.nextSubject = new MutableLiveData<>();

//        this.fetchCurrentAndNextSubjects(userUid);

        // ###
        // ### Fetch pending tasks and exams.
        // ###

        if (userRoleNode.equals("students")) {
            this.pendingTasks = new MutableLiveData<>();
            this.pendingExams = new MutableLiveData<>();
            this.fetchStudentPendingTasksAndExams(userUid, subjectsViewModel);
        } else {
            this.teacherPendingTasks = new MutableLiveData<>();
            this.teacherPendingExams = new MutableLiveData<>();
            this.fetchTeacherPendingTasksAndExams(subjectsViewModel);

            this.teacherAttendanceStatus = new MutableLiveData<>();
            this.fetchTeacherAttendanceStatus();
        }
    }

    void fetchCurrentAndNextSubjects2(final HashMap<String, Subject> subjects) {
        final LocalDateTime now = LocalDateTime.now();

        final String dayOfWeek = now.getDayOfWeek().toString().toLowerCase();
        final LocalTime currentLocalTime = now.toLocalTime();

        // If there are, get the current hour range ID to see if there's a subject right now.
        FirebaseDatabase.getInstance()
                .getReference("2wiam/timetable2")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot timetableSnapshot) {
                        DataSnapshot weekDaySnapshot = timetableSnapshot.child("days/" + dayOfWeek);

                        DateTimeFormatter hourMinuteFormatter = DateTimeFormatter.ofPattern("HH:mm");

                        ArrayList<LocalTime> hoursBegin = new ArrayList<>();
                        ArrayList<LocalTime> hoursEnd = new ArrayList<>();

                        for (DataSnapshot hourRangeSnapshot : timetableSnapshot.child("hours").getChildren()) {
                            String[] hours = hourRangeSnapshot
                                    .getValue(String.class)
                                    .split(" - ")
                            ;
                            hoursBegin.add(LocalTime.parse(
                                    hours[0],
                                    hourMinuteFormatter
                            ));
                            hoursEnd.add(LocalTime.parse(
                                    hours[1],
                                    hourMinuteFormatter
                            ));
                        }

                        boolean found = false;

                        for (int i = 0, size = (int) weekDaySnapshot.getChildrenCount(); i < size && !found; i += 1) {
                            Subject possibleCurrentSubject = subjects.get(weekDaySnapshot.child(
                                    Integer.toString(i + 1)
                            ).getValue(String.class));

                            if (
                                    currentLocalTime.isAfter(hoursBegin.get(i))
                                    && currentLocalTime.isBefore(hoursEnd.get(i))
                                    // Subject might be null, for example, in the case of "Tutoria".
                                    && possibleCurrentSubject != null
                                    && possibleCurrentSubject.isVisibleAndAvailableForCurrentUser()
                            ) {
                                // If there is, that's the current subject.
                                HomeViewModel.this.currentSubject.setValue(
                                        Optional.of(new SubjectCardInfo(
                                                possibleCurrentSubject,
                                                hoursBegin.get(i).toString(),
                                                hoursEnd.get(i).toString()
                                        ))
                                );

                                // Now, check if there's a subject after this one.
                                boolean nextSubjectFound = false;

                                for (int j = i+1; j < size && !nextSubjectFound; j += 1) {
                                    Subject possibleNextSubject = subjects.get(weekDaySnapshot.child(
                                            Integer.toString(j + 1)
                                    ).getValue(String.class));

                                    if (
                                            possibleNextSubject != null
                                            && possibleNextSubject.isVisibleAndAvailableForCurrentUser()
                                    ) {
                                        HomeViewModel.this.nextSubject.setValue(
                                                Optional.of(new SubjectCardInfo(
                                                        possibleNextSubject,
                                                        hoursBegin.get(j).toString(),
                                                        hoursEnd.get(j).toString()
                                                ))
                                        );

                                        nextSubjectFound = true;
                                    }
                                }

                                // If none was found, there's none (wow).
                                if (!nextSubjectFound) {
                                    HomeViewModel.this.nextSubject.setValue(
                                            Optional.<SubjectCardInfo>empty()
                                    );
                                }

                                found = true;
                            }
                        }

                        // If there's no current subject, there might be a next subject later today.
                        if (!found) {
                            HomeViewModel.this.currentSubject.setValue(
                                    Optional.<SubjectCardInfo>empty()
                            );

                            boolean nextSubjectFound = false;

                            for (int i = 0, size = (int) weekDaySnapshot.getChildrenCount(); i < size && !nextSubjectFound; i += 1) {
//                                DataSnapshot possibleNextSubject = weekDaySnapshot.child(
//                                        Integer.toString(i + 1)
//                                );
                                Subject possibleNextSubject = subjects.get(weekDaySnapshot.child(
                                        Integer.toString(i + 1)
                                ).getValue(String.class));

                                if (
                                        currentLocalTime.isBefore(hoursBegin.get(i))
                                        && possibleNextSubject != null
                                        && possibleNextSubject.isVisibleAndAvailableForCurrentUser()
                                ) {
                                    HomeViewModel.this.nextSubject.setValue(
                                            Optional.of(new SubjectCardInfo(
                                                    possibleNextSubject,
                                                    hoursBegin.get(i).toString(),
                                                    hoursEnd.get(i).toString()
                                            ))
                                    );

                                    nextSubjectFound = true;
                                }
                            }

                            if (!nextSubjectFound) {
                                HomeViewModel.this.nextSubject.setValue(
                                        Optional.<SubjectCardInfo>empty()
                                );
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                })
        ;
    }

    private void fetchCurrentAndNextSubjects(String userUid) {
        final LocalDateTime now = LocalDateTime.now();

        final String dayOfWeek = now.getDayOfWeek().toString().toLowerCase();
        final LocalTime currentLocalTime = now.toLocalTime();

        FirebaseDatabase.getInstance()
                .getReference("2wiam/userTimetable/" + userUid + "/" + dayOfWeek)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull final DataSnapshot userTimetableSnapshot) {
                        // Check if there are subjects today.
                        if (!userTimetableSnapshot.exists()) {
                            HomeViewModel.this.currentSubject.setValue(
                                    Optional.<SubjectCardInfo>empty()
                            );
                            HomeViewModel.this.nextSubject.setValue(
                                    Optional.<SubjectCardInfo>empty()
                            );

                        } else {
                            // If there are, get the current hour range ID to see if there's a subject right now.
                            FirebaseDatabase.getInstance()
                                    .getReference("2wiam/timetable/hours")
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            DateTimeFormatter hourMinuteFormatter = DateTimeFormatter.ofPattern("HH:mm");

                                            ArrayList<LocalTime> hoursBegin = new ArrayList<>();
                                            ArrayList<LocalTime> hoursEnd = new ArrayList<>();

                                            for (DataSnapshot hourRangeSnapshot : dataSnapshot.getChildren()) {
                                                String[] hours = hourRangeSnapshot
                                                        .getValue(String.class)
                                                        .split(" - ")
                                                ;
                                                hoursBegin.add(LocalTime.parse(
                                                        hours[0],
                                                        hourMinuteFormatter
                                                ));
                                                hoursEnd.add(LocalTime.parse(
                                                        hours[1],
                                                        hourMinuteFormatter
                                                ));
                                            }

                                            boolean found = false;

                                            for (int i = 0, size = hoursBegin.size(); i < size && !found; i += 1) {
                                                DataSnapshot possibleCurrentSubject = userTimetableSnapshot.child(
                                                        Integer.toString(i + 1)
                                                );

                                                if (
                                                        currentLocalTime.isAfter(hoursBegin.get(i))
                                                        && currentLocalTime.isBefore(hoursEnd.get(i))
                                                        && possibleCurrentSubject.exists()
                                                ) {
                                                    // If there is, that's the current subject.
                                                    HomeViewModel.this.currentSubject.setValue(
                                                            Optional.of(new SubjectCardInfo(
                                                                possibleCurrentSubject.getValue(String.class).toUpperCase(),
                                                                hoursBegin.get(i).toString(),
                                                                hoursEnd.get(i).toString()
                                                            ))
                                                    );

                                                    // Now, check if there's a subject after this one.
                                                    boolean nextSubjectFound = false;

                                                    for (int j = i+1; j < size && !nextSubjectFound; j += 1) {
                                                        DataSnapshot possibleNextSubject = userTimetableSnapshot.child(
                                                                Integer.toString(j + 1)
                                                        );
                                                        if (possibleNextSubject.exists()) {
                                                            HomeViewModel.this.nextSubject.setValue(
                                                                    Optional.of(new SubjectCardInfo(
                                                                            possibleNextSubject.getValue(String.class).toUpperCase(),
                                                                            hoursBegin.get(j).toString(),
                                                                            hoursEnd.get(j).toString()
                                                                    ))
                                                            );

                                                            nextSubjectFound = true;
                                                        }
                                                    }

                                                    // If none was found, there's none (wow).
                                                    if (!nextSubjectFound) {
                                                        HomeViewModel.this.nextSubject.setValue(
                                                                Optional.<SubjectCardInfo>empty()
                                                        );
                                                    }

                                                    found = true;
                                                }
                                            }

                                            // If there's no current subject, there might be a next subject later today.
                                            if (!found) {
                                                HomeViewModel.this.currentSubject.setValue(
                                                        Optional.<SubjectCardInfo>empty()
                                                );

                                                boolean nextSubjectFound = false;

                                                for (int i = 0, size = hoursBegin.size(); i < size && !nextSubjectFound; i += 1) {
                                                    DataSnapshot possibleNextSubject = userTimetableSnapshot.child(
                                                            Integer.toString(i + 1)
                                                    );

                                                    if (
                                                            currentLocalTime.isBefore(hoursBegin.get(i))
                                                            && possibleNextSubject.exists()
                                                    ) {
                                                        HomeViewModel.this.nextSubject.setValue(
                                                                Optional.of(new SubjectCardInfo(
                                                                        possibleNextSubject.getValue(String.class).toUpperCase(),
                                                                        hoursBegin.get(i).toString(),
                                                                        hoursEnd.get(i).toString()
                                                                ))
                                                        );

                                                        nextSubjectFound = true;
                                                    }
                                                }

                                                if (!nextSubjectFound) {
                                                    HomeViewModel.this.nextSubject.setValue(
                                                            Optional.<SubjectCardInfo>empty()
                                                    );
                                                }
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    })
                            ;
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
        ;
    }

    private void fetchStudentPendingTasksAndExams(final String userUid, SubjectsViewModel subjectsViewModel) {
        subjectsViewModel.getSubjects().observeForever(new Observer<HashMap<String, Subject>>() {
            @Override
            public void onChanged(final HashMap<String, Subject> subjects) {
                // Fetch all the user's uploads, which will be used to know what tasks haven't been submitted.
                FirebaseDatabase.getInstance().getReference("2wiam/uploads/" + userUid)
                        .addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull final DataSnapshot userUploadsSnapshot) {
                                // Check every task and exam of every Topic of every Subject.
                                // Only considering visible and available Topics and Subjects.
                                FirebaseDatabase.getInstance()
                                        .getReference("2wiam/topicItems")
                                        .addValueEventListener(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot topicItemsSnapshot) {
                                                final HashMap<String, ArrayList<Task>> pendingTasks = new HashMap<>();
                                                final HashMap<String, ArrayList<Task>> pendingExams = new HashMap<>();

                                                boolean foundTasks = false;
                                                boolean foundExams = false;

                                                for (Subject subject : subjects.values()) {
                                                    Log.d("TASKS", "Found subject " + subject.getId());

                                                    if (subject.isAvailableForCurrentUser() && subject.isVisible()) {
                                                        final String subjectId = subject.getId().toLowerCase();

                                                        final ArrayList<Task> subjectTasks = new ArrayList<>();
                                                        final ArrayList<Task> subjectExams = new ArrayList<>();

                                                        Log.d("TASKS", "Subject " + subjectId + " is available, processing topics");

                                                        for (Topic topic : subject.getTopics()) {
                                                            Log.d("TASKS", "Found " + subjectId + " topic " + topic.getId());

                                                            if (topic.isAvailableForCurrentUser() && topic.isVisible()) {
                                                                final String topicId = topic.getId().toLowerCase();

                                                                Log.d("TASKS", subjectId + " topic " + topicId + " is available, processing tasks");

                                                                // Search for pending tasks.
                                                                for (DataSnapshot taskSnapshot : topicItemsSnapshot.child(String.format("/%s/%s/tasks", subjectId, topicId)).getChildren()) {
                                                                    Log.d("TASKS", "Found " + subjectId + "/" + topicId + " task " + taskSnapshot.getKey());

                                                                    final Task task = taskSnapshot.getValue(Task.class);
                                                                    task.setId(taskSnapshot.getKey());

                                                                    if (task.isVisible()) {
                                                                        task.setSubjectId(subjectId);
                                                                        task.setTopicId(topicId);

                                                                        // Check if the current user has made a submission for that Task.
                                                                        // If there's none, add this Task to the pendingTasks list.
                                                                        if (!userUploadsSnapshot.child(String.format("%s/%s/tasks/%s", subjectId, topicId, task.getId())).exists()) {
                                                                            Log.d("TASKS", subjectId + "/" + topicId + " task " + task.getId() + " not submitted, adding it");

                                                                            subjectTasks.add(task);

                                                                        } else {
                                                                            Log.d("TASKS", subjectId + "/" + topicId + " task " + task.getId() + " submitted, skipping");
                                                                        }

                                                                    } else {
                                                                        Log.d("TASKS", subjectId + "/" + topicId + " task " + task.getId() + " not visible, skipping");
                                                                    }
                                                                }

                                                                if (!subjectTasks.isEmpty()) {
                                                                    foundTasks = true;
                                                                    pendingTasks.put(subjectId, subjectTasks);
                                                                }

                                                                HomeViewModel.this.pendingTasks.setValue(pendingTasks);

                                                                // Search for pending exams.
                                                                for (DataSnapshot examSnapshot : topicItemsSnapshot.child(String.format("/%s/%s/exams", subjectId, topicId)).getChildren()) {
                                                                    Log.d("TASKS", "Found " + subjectId + "/" + topicId + " exam " + examSnapshot.getKey());

                                                                    Task exam = examSnapshot.getValue(Task.class);
                                                                    exam.setId(examSnapshot.getKey());

                                                                    if (exam.isVisible()) {
                                                                        exam.setSubjectId(subjectId);
                                                                        exam.setTopicId(topicId);

                                                                        if (!userUploadsSnapshot.child(String.format("%s/%s/exams/%s", subjectId, topicId, exam.getId())).exists()) {
                                                                            Log.d("TASKS", subjectId + "/" + topicId + " exam " + exam.getId() + " not submitted, adding it");

                                                                            subjectExams.add(exam);

                                                                        } else {
                                                                            Log.d("TASKS", subjectId + "/" + topicId + " exam " + exam.getId() + " submitted, skipping");
                                                                        }

                                                                    } else {
                                                                        Log.d("TASKS", subjectId + "/" + topicId + " exam " + exam.getId() + " not visible, skipping");
                                                                    }
                                                                }

                                                                if (!subjectExams.isEmpty()) {
                                                                    foundExams = true;
                                                                    pendingExams.put(subjectId, subjectExams);
                                                                }

                                                                HomeViewModel.this.pendingExams.setValue(pendingExams);
                                                            }
                                                        }
                                                    }
                                                }

                                                // If no tasks or exams were found because there isn't
                                                // any available Topics, put an empty HashMap.
                                                if (!foundTasks) {
                                                    HomeViewModel.this.pendingTasks.setValue(new HashMap<String, ArrayList<Task>>());
                                                }

                                                if (!foundExams) {
                                                    HomeViewModel.this.pendingExams.setValue(new HashMap<String, ArrayList<Task>>());
                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                            }
                                        })
                                ;
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        })
                ;
            }
        });
    }

    private void fetchTeacherPendingTasksAndExams(SubjectsViewModel subjectsViewModel) {
        Observer<HashMap<String, Subject>> observer = new Observer<HashMap<String, Subject>>() {
            int tasksCount = 0;
            int examsCount = 0;

            @Override
            public void onChanged(HashMap<String, Subject> stringSubjectHashMap) {
                HomeViewModel.this.teacherPendingTasks.setValue(tasksCount);
                HomeViewModel.this.teacherPendingExams.setValue(examsCount);

                for (Subject subject : stringSubjectHashMap.values()) {
                    if (subject.isAvailableForCurrentUser() && subject.isVisible()) {
                        final String subjectId = subject.getId().toLowerCase();

                        for (Topic topic : subject.getTopics()) {
                            if (topic.isVisible()) {
                                final String topicId = topic.getId().toLowerCase();

                                FirebaseDatabase.getInstance().getReference(String.format(
                                        "2wiam/subjects/%s/topics/%s/students", subjectId, topicId
                                )).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot studentsSnapshot) {
                                        // For each student enrolled in the current Topic,
                                        // count how many tasks and exams s/he has submitted and
                                        // haven't been graded yet.
                                        for (DataSnapshot studentIdSnapshot : studentsSnapshot.getChildren()) {
                                            final String studentId = studentIdSnapshot.getKey();

                                            FirebaseDatabase.getInstance().getReference(String.format(
                                                    "2wiam/uploads/%s/%s/%s", studentId, subjectId, topicId
                                            )).addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot topicUploads) {
                                                    for (DataSnapshot taskUpload : topicUploads.child("tasks").getChildren()) {
                                                        if (!taskUpload.child("grading").exists()) {
                                                            tasksCount += 1;
                                                            HomeViewModel.this.teacherPendingTasks.setValue(tasksCount);
                                                        }
                                                    }

                                                    for (DataSnapshot examUpload : topicUploads.child("exams").getChildren()) {
                                                        if (!examUpload.child("grading").exists()) {
                                                            examsCount += 1;
                                                            HomeViewModel.this.teacherPendingExams.setValue(examsCount);
                                                        }
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                }
                                            });
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                            }
                        }
                    }
                }
            }
        };

        subjectsViewModel.getSubjects().observeForever(observer);
    }

    private void fetchTeacherAttendanceStatus() {
        this.currentSubject.observeForever(new Observer<Optional<SubjectCardInfo>>() {
            @Override
            public void onChanged(Optional<SubjectCardInfo> subjectCardInfo) {
                if (subjectCardInfo.isPresent()) {
                    String localDateNow = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy_MM_dd"));
                    String hourBegin = subjectCardInfo.get().hourBegin.replace(':', '_');

                    final String subjectId = subjectCardInfo.get().subjectId.toLowerCase();
                    final String path = String.format(
                            "2wiam/attendance/%s/%s__%s", subjectId, localDateNow, hourBegin
                    );
                    FirebaseDatabase.getInstance().getReference(path)
                            .addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot attendanceSnapshot) {
                                    if (attendanceSnapshot.exists()) {
                                        HomeViewModel.this.teacherAttendanceStatus.setValue(
                                                Optional.of(new String[]{path})
                                        );
                                    } else {
                                        HomeViewModel.this.teacherAttendanceStatus.setValue(
                                                Optional.of(new String[]{path, subjectId})
                                        );
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            })
                    ;
                } else {
                    HomeViewModel.this.teacherAttendanceStatus.setValue(
                            Optional.<String[]>empty()
                    );
                }
            }
        });
    }

    LiveData<Optional<SubjectCardInfo>> getCurrentSubject() {
        return this.currentSubject;
    }

    LiveData<Optional<SubjectCardInfo>> getNextSubject() {
        return this.nextSubject;
    }

    LiveData<int[]> getUnreadChatsAndMessagesCount() {
        return this.unreadChatsAndMessagesCount;
    }

    LiveData<HashMap<String, ArrayList<Task>>> getPendingTasks() {
        return this.pendingTasks;
    }

    LiveData<HashMap<String, ArrayList<Task>>> getPendingExams() {
        return this.pendingExams;
    }

    LiveData<Integer> getTeacherPendingTasks() {
        return this.teacherPendingTasks;
    }

    LiveData<Integer> getTeacherPendingExams() {
        return this.teacherPendingExams;
    }

    LiveData<Optional<String[]>> getTeacherAttendanceStatus() {
        return this.teacherAttendanceStatus;
    }

    static class SubjectCardInfo {
        private String subjectId;
        private String color;
        private String hourBegin;
        private String hourEnd;

        SubjectCardInfo(String subjectId, String hourBegin, String hourEnd) {
            this.subjectId = subjectId;
            this.hourBegin = hourBegin;
            this.hourEnd = hourEnd;
        }

        SubjectCardInfo(Subject subject, String hourBegin, String hourEnd) {
            this.subjectId = subject.getId().toUpperCase();
            this.color = subject.getColor();
            this.hourBegin = hourBegin;
            this.hourEnd = hourEnd;
        }

        String getSubjectId() {
            return this.subjectId;
        }

        String getHourBegin() {
            return this.hourBegin;
        }

        String getHourEnd() {
            return this.hourEnd;
        }
    }
}