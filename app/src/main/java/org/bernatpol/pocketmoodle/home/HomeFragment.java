package org.bernatpol.pocketmoodle.home;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import org.bernatpol.pocketmoodle.R;
import org.bernatpol.pocketmoodle.subject.Subject;
import org.bernatpol.pocketmoodle.subject.SubjectsViewModel;
import org.bernatpol.pocketmoodle.task.Task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class HomeFragment extends Fragment {
    private HomeViewModel homeViewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.getContext());
        String userRoleNode = sharedPreferences.getString("userRoleNode", "students");
        boolean student = sharedPreferences.getBoolean("student", true);

        //Se coje SubjectsViewModel de la MainActivity, porque sabiamos que existe.
        SubjectsViewModel subjectsViewModel = new ViewModelProvider(
                requireActivity()
        ).get(SubjectsViewModel.class);

        this.homeViewModel = new ViewModelProvider(
                this,
                new HomeViewModelFactory(userRoleNode, subjectsViewModel)
        ).get(HomeViewModel.class);

        View root = inflater.inflate(R.layout.fragment_home, container, false);

        if (student) {
            this.addUnreadMessagesCount(root);
            this.addStudentPendingTasksCount(root);
            this.addStudentPendingExamsCount(root);
//            this.addUnexcusedAbsencesCount(root);
        } else {
            this.addUnreadMessagesCount(root);
            this.addTeacherPendingTasksCount(root);
            this.addTeacherPendingExamsCount(root);
            this.addTeacherAttendanceStatus(root);
        }

        subjectsViewModel.getSubjects().observe(getViewLifecycleOwner(), new Observer<HashMap<String, Subject>>() {
            @Override
            public void onChanged(HashMap<String, Subject> subjects) {
                HomeFragment.this.homeViewModel.fetchCurrentAndNextSubjects2(subjects);
            }
        });
        this.fillSubjectCard(this.homeViewModel.getCurrentSubject(), root.findViewById(R.id.current_subject), R.string.now, subjectsViewModel);
        this.fillSubjectCard(this.homeViewModel.getNextSubject(), root.findViewById(R.id.next_subject), R.string.later, subjectsViewModel);

        return root;
    }

    private void fillSubjectCard(
            LiveData<Optional<HomeViewModel.SubjectCardInfo>> subjectCardLiveData,
            final View subjectCard,
            int stringResource,
            final SubjectsViewModel subjectsViewModel
    ) {
        ((TextView) subjectCard.findViewById(R.id.when))
                .setText(stringResource)
        ;

        subjectCardLiveData.observe(getViewLifecycleOwner(), new Observer<Optional<HomeViewModel.SubjectCardInfo>>() {
            @Override
            public void onChanged(final Optional<HomeViewModel.SubjectCardInfo> subjectCardInfo) {
                if (subjectCardInfo.isPresent()) {
                    ((TextView) subjectCard.findViewById(R.id.subject_id))
                            .setText(subjectCardInfo.get().getSubjectId())
                    ;
                    ((TextView) subjectCard.findViewById(R.id.hour_begin))
                            .setText(subjectCardInfo.get().getHourBegin())
                    ;
                    ((TextView) subjectCard.findViewById(R.id.hour_end))
                            .setText(subjectCardInfo.get().getHourEnd())
                    ;

                    subjectsViewModel.getSubjects().observe(getViewLifecycleOwner(), new Observer<HashMap<String, Subject>>() {
                        @Override
                        public void onChanged(HashMap<String, Subject> stringSubjectHashMap) {
                            subjectCard.setBackgroundColor(Color.parseColor(
                                    stringSubjectHashMap.get(subjectCardInfo.get().getSubjectId().toLowerCase()).getColor()
                            ));
                        }
                    });

                    //cadr_view_subject
                    subjectCard.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Bundle bundle = new Bundle();
                            bundle.putString("subjectId", subjectCardInfo.get().getSubjectId());

                            NavHostFragment.findNavController(HomeFragment.this)
                                    .navigate(R.id.action_nav_home_to_nav_subject_details, bundle)
                            ;
                        }
                    });

                } else {
                    ((TextView) subjectCard.findViewById(R.id.subject_id))
                            .setText(R.string.nothing)
                    ;
                    ((TextView) subjectCard.findViewById(R.id.hour_begin))
                            .setText("--:--")
                    ;
                    ((TextView) subjectCard.findViewById(R.id.hour_end))
                            .setText("--:--")
                    ;
                    subjectCard.setBackgroundColor(Color.parseColor("#00000000"));
                    subjectCard.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(HomeFragment.this.getContext(), "Nothing!!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }

    private View getCardIndicator(int iconResource) {
        CardView container = new CardView(this.getContext());

        View view = LayoutInflater
                .from(this.getContext())
                .inflate(R.layout.card_indicator, container, false);
        ((ImageView) view.findViewById(R.id.icon)).setImageResource(iconResource);

        return view;
    }

    private void setValues(View container, String title, String subtitle, boolean needsAttention) {
        ((TextView) container.findViewById(R.id.title)).setText(title);
        ((TextView) container.findViewById(R.id.subtitle)).setText(subtitle);

        ImageView icon = container.findViewById(R.id.icon);

        // Cambiar el color de la imagen
//        icon.setColorFilter(

        // Cambiar el color del fondo
        icon.setBackgroundColor(
                getResources().getColor(
                        needsAttention ? R.color.lightRed : R.color.lightGreen
                )
        );
        icon.setColorFilter(
                getResources().getColor(R.color.white)
        );
    }

    private void addUnreadMessagesCount(View root) {
        final View unreadMessages = this.getCardIndicator(R.drawable.ic_message_24dp);

        this.homeViewModel.getUnreadChatsAndMessagesCount().observe(getViewLifecycleOwner(), new Observer<int[]>() {
            @Override
            public void onChanged(int[] unreadChatsAndMessagesCount) {
                int unreadChatsCount = unreadChatsAndMessagesCount[0];
                int unreadMessagesCount = unreadChatsAndMessagesCount[1];

                if (unreadChatsCount == 0) {
                    HomeFragment.this.setValues(
                            unreadMessages,
                            "No unread messages",
                            "Nothing to see here!",
                            false
                    );
                } else {
                    HomeFragment.this.setValues(
                            unreadMessages,
                            String.format("%d unread messages", unreadMessagesCount),
                            String.format("From %s chats", unreadChatsCount),
                            true
                    );
                }
            }
        });

        unreadMessages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavHostFragment.findNavController(HomeFragment.this)
                        .navigate(R.id.action_nav_home_to_nav_chats)
                ;
            }
        });

        ((LinearLayout) root.findViewById(R.id.root_container)).addView(unreadMessages);
    }

    private void addStudentPendingTasksCount(final View root) {
        final View upcomingTasks = HomeFragment.this.getCardIndicator(R.drawable.ic_library_books_24dp);

        this.homeViewModel.getPendingTasks().observe(getViewLifecycleOwner(), new Observer<HashMap<String, ArrayList<Task>>>() {
            @Override
            public void onChanged(HashMap<String, ArrayList<Task>> pendingTasks) {
                if (pendingTasks.isEmpty()) {
                    HomeFragment.this.setValues(
                            upcomingTasks,
                            "No upcoming tasks",
                            "All done for now!",
                            false
                    );

                    upcomingTasks.setOnClickListener(null);

                } else {
                    final ArrayList<Task> tasks = new ArrayList<>();

                    for (Map.Entry<String, ArrayList<Task>> entry : pendingTasks.entrySet()) {
                        tasks.addAll(entry.getValue());
                    }

                    tasks.sort(Task.TASK_DATE_END_COMPARATOR);

                    HomeFragment.this.setValues(
                            upcomingTasks,
                            String.format("%d pending tasks", tasks.size()),
                            String.format("From %d subjects", pendingTasks.size()),
                            true
                    );

                    upcomingTasks.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Bundle bundle = new Bundle();
                            bundle.putString("title", getResources().getString(R.string.tasks));
                            bundle.putParcelableArrayList("tasks", tasks);

                            NavHostFragment.findNavController(HomeFragment.this)
                                    .navigate(R.id.action_nav_home_to_nav_tasks_list, bundle)
                            ;
                        }
                    });
                }
            }
        });

        ((LinearLayout) root.findViewById(R.id.root_container)).addView(upcomingTasks);
    }

    private void addStudentPendingExamsCount(View root) {
        final View upcomingExams = this.getCardIndicator(R.drawable.ic_exam);

        this.homeViewModel.getPendingExams().observe(getViewLifecycleOwner(), new Observer<HashMap<String, ArrayList<Task>>>() {
            @Override
            public void onChanged(HashMap<String, ArrayList<Task>> pendingExams) {
                if (pendingExams.isEmpty()) {
                    HomeFragment.this.setValues(
                            upcomingExams,
                            "No pending exams",
                            "All done for now!",
                            false
                    );

                    upcomingExams.setOnClickListener(null);

                } else {
                    final ArrayList<Task> exams = new ArrayList<>();

                    for (Map.Entry<String, ArrayList<Task>> entry : pendingExams.entrySet()) {
                        exams.addAll(entry.getValue());
                    }

                    exams.sort(Task.TASK_DATE_END_COMPARATOR);

                    HomeFragment.this.setValues(
                            upcomingExams,
                            String.format("%d pending exams", exams.size()),
                            String.format("From %d subjects", pendingExams.size()),
                            true
                    );

                    upcomingExams.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Bundle bundle = new Bundle();
                            bundle.putString("title", getResources().getString(R.string.exams));
                            bundle.putParcelableArrayList("tasks", exams);

                            NavHostFragment.findNavController(HomeFragment.this)
                                    .navigate(R.id.action_nav_home_to_nav_tasks_list, bundle)
                            ;
                        }
                    });
                }
            }
        });

        ((LinearLayout) root.findViewById(R.id.root_container)).addView(upcomingExams);
    }

//    private void addUnexcusedAbsencesCount(View root) {
//        View unexcusedAbsences = this.getCardIndicator(R.drawable.ic_directions_run);
//
//        int unexcusedAbsencesCount = PlaceholderServer.getPlaceholderCount();
//        if (unexcusedAbsencesCount == 0) {
//            this.setValues(
//                    unexcusedAbsences,
//                    "No unexcused absences",
//                    "Keep it that way!",
//                    false
//            );
//        } else {
//            this.setValues(
//                    unexcusedAbsences,
//                    String.format("%d unexcused absences", unexcusedAbsencesCount),
//                    "$num more and you will be expelled. ^_^",
//                    true
//            );
//        }
//
//        ((LinearLayout) root.findViewById(R.id.root_container)).addView(unexcusedAbsences);
//    }

    private void addTeacherPendingTasksCount(View root) {
        final View pendingTasks = this.getCardIndicator(R.drawable.ic_library_books_24dp);

        this.homeViewModel.getTeacherPendingTasks().observe(getViewLifecycleOwner(), new Observer<Integer>() {
            @Override
            public void onChanged(Integer pendingTasksCount) {
                if (pendingTasksCount == 0) {
                    HomeFragment.this.setValues(
                            pendingTasks,
                            "No pending tasks",
                            "Be ready for more!",
                            false
                    );

                } else {
                    HomeFragment.this.setValues(
                            pendingTasks,
                            String.format(
                                    "%d %s",
                                    pendingTasksCount,
                                    getResources().getString(R.string.pending_tasks).toLowerCase()
                            ),
                            "Hurry up!",
                            true
                    );
                }
            }
        });

        ((LinearLayout) root.findViewById(R.id.root_container)).addView(pendingTasks);
    }

    private void addTeacherPendingExamsCount(View root) {
        final View pendingExams = this.getCardIndicator(R.drawable.ic_exam);

        homeViewModel.getTeacherPendingExams().observe(getViewLifecycleOwner(), new Observer<Integer>() {
            @Override
            public void onChanged(Integer pendingExamsCount) {
                if (pendingExamsCount == 0) {
                    HomeFragment.this.setValues(
                            pendingExams,
                            "No pending exams",
                            "Be ready for more!",
                            false
                    );

                } else {
                    HomeFragment.this.setValues(
                            pendingExams,
                            String.format(
                                    "%d %s",
                                    pendingExamsCount,
                                    getResources().getString(R.string.pending_exams).toLowerCase()
                            ),
                            "Hurry up!",
                            true
                    );
                }
            }
        });

        ((LinearLayout) root.findViewById(R.id.root_container)).addView(pendingExams);
    }

    private void addTeacherAttendanceStatus(View root) {
        final View attendanceStatus = this.getCardIndicator(R.drawable.ic_directions_run);

        homeViewModel.getTeacherAttendanceStatus().observe(getViewLifecycleOwner(), new Observer<Optional<String[]>>() {
            @Override
            public void onChanged(final Optional<String[]> status) {
                if (status.isPresent()) {
                    if (status.get().length == 2) {
                        HomeFragment.this.setValues(
                                attendanceStatus,
                                "Attendance not taken",
                                "Tap to take",
                                true
                        );

                        attendanceStatus.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Bundle bundle = new Bundle();
                                // Attendance data will be saved at $path.
                                bundle.putString("path", status.get()[0]);
                                // Fetch users from the current Topic of the current Subject.
                                bundle.putString("subjectId", status.get()[1]);

                                NavHostFragment.findNavController(HomeFragment.this)
                                        .navigate(R.id.action_nav_home_to_nav_attendance, bundle)
                                ;
                            }
                        });

                    } else if (status.get().length == 1) {
                        HomeFragment.this.setValues(
                                attendanceStatus,
                                "Attendance already taken",
                                "Tap to edit",
                                false
                        );

                        attendanceStatus.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Bundle bundle = new Bundle();
                                // Fetch users already present at $path in Firebase.
                                bundle.putString("path", status.get()[0]);

                                NavHostFragment.findNavController(HomeFragment.this)
                                        .navigate(R.id.action_nav_home_to_nav_attendance, bundle)
                                ;
                            }
                        });
                    }

                } else {
                    HomeFragment.this.setValues(
                            attendanceStatus,
                            "Nothing right now",
                            "Relax",
                            false
                    );
                }
            }
        });

        ((LinearLayout) root.findViewById(R.id.root_container)).addView(attendanceStatus);
    }
}
