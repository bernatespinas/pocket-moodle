package org.bernatpol.pocketmoodle.home;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import org.bernatpol.pocketmoodle.subject.SubjectsViewModel;

class HomeViewModelFactory implements ViewModelProvider.Factory {
    private String userRoleNode;
    private SubjectsViewModel subjectsViewModel;

    HomeViewModelFactory(String userRoleNode, SubjectsViewModel subjectsViewModel) {
        this.userRoleNode = userRoleNode;
        this.subjectsViewModel = subjectsViewModel;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new HomeViewModel(this.userRoleNode, this.subjectsViewModel);
    }
}
