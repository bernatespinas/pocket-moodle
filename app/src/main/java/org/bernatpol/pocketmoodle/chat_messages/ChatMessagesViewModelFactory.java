package org.bernatpol.pocketmoodle.chat_messages;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import org.bernatpol.pocketmoodle.chat.Chat;

class ChatMessagesViewModelFactory implements ViewModelProvider.Factory {
    private Chat chat;
    private String userFullName;
    private String userPicture;
    private String userRoleNode;

    ChatMessagesViewModelFactory(Chat chat, String userFullName, String userPicture, String userRoleNode) {
        this.chat = chat;
        this.userFullName = userFullName;
        this.userPicture = userPicture;
        this.userRoleNode = userRoleNode;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new ChatMessagesViewModel(this.chat, this.userFullName ,this.userPicture, this.userRoleNode);
    }
}
