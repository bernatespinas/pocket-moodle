package org.bernatpol.pocketmoodle.chat_messages;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.bernatpol.pocketmoodle.chat.Chat;

import java.util.ArrayList;

class ChatMessagesViewModel extends ViewModel {
    private MutableLiveData<ArrayList<ChatMessage>> messages;

    private String userId;
    private Chat chat;

    private String userFullName;
    private String userPicture;
    private String userRoleNode;

    ChatMessagesViewModel(Chat chat, String userFullName, String userPicture, String userRoleNode) {
        this.messages = new MutableLiveData<>();
        this.messages.setValue(new ArrayList<ChatMessage>(5));

        this.userId = FirebaseAuth.getInstance().getCurrentUser().getUid();

        this.chat = chat;

        this.userFullName = userFullName;
        this.userPicture = userPicture;
        this.userRoleNode = userRoleNode;

        // When opening a chat, reset the current user's unread messages' count.
        this.resetUnreadMessagesCount();

        // Fetch chats ordered by timestamp.
        FirebaseDatabase.getInstance()
                .getReference("2wiam/chats/" + this.chat.getId())
                .orderByChild("timestamp")
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        ChatMessage chatMessage = dataSnapshot.getValue(ChatMessage.class);

                        ArrayList<ChatMessage> chatMessages =
                                ChatMessagesViewModel.this.messages.getValue()
                        ;

                        chatMessages.add(chatMessage);

                        ChatMessagesViewModel.this.messages.setValue(chatMessages);
                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                })
        ;

        this.userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    // Reset a chat's unread messages' count to 0.
    private void resetUnreadMessagesCount() {
        // Reset the local chat's unread messages count.
        this.chat.setUnreadMessages(0);

        final DatabaseReference unreadMessagesReference =
                FirebaseDatabase.getInstance().getReference(String.format(
                    "2wiam/%s/%s/chats/%s/unreadMessages", userRoleNode, this.userId, this.chat.getId()
                ))
        ;

        // Reset the Firebase chat's
        unreadMessagesReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    unreadMessagesReference.setValue(0);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    LiveData<ArrayList<ChatMessage>> getMessages() {
        return this.messages;
    }

    void sendMessage(final ChatMessage chatMessage) {
        FirebaseDatabase.getInstance()
                .getReference("2wiam/chats/" + this.chat.getId())
                .push()
                .setValue(chatMessage)
        ;

        this.updateCurrentUserLastMessage(chatMessage);
        this.updateOtherUserLastMessage(chatMessage);

    }

    private void updateCurrentUserLastMessage(ChatMessage chatMessage) {
        // Update message info.
        this.chat.setLastMessage(chatMessage.getText());
        this.chat.setTime(chatMessage.getTime());
        this.chat.setTimestamp(chatMessage.getTimestamp());

        FirebaseDatabase.getInstance().getReference(String.format(
                "2wiam/%s/%s/chats/%s", this.userRoleNode, this.userId, this.chat.getId()
        )).setValue(chat);
    }

    private void updateOtherUserLastMessage(final ChatMessage chatMessage) {
        final DatabaseReference otherChatReference = FirebaseDatabase.getInstance().getReference(String.format(
                "2wiam/%s/%s/chats/%s", this.chat.getOtherRoleNode(), this.chat.getOtherId(), this.chat.getId()
        ));

        otherChatReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot otherChatSnapshot) {
                if (otherChatSnapshot.exists()) {
                    // If this chat already exists, update its message info and increment its
                    // unread messages' count.
                    Chat otherChat = otherChatSnapshot.getValue(Chat.class);
                    otherChat.setLastMessage(chatMessage.getText());
                    otherChat.setTime(chatMessage.getTime());
                    otherChat.setTimestamp(chatMessage.getTimestamp());
                    otherChat.setUnreadMessages(otherChat.getUnreadMessages()+1);

                    otherChatReference.setValue(otherChat);

                } else {
                    // If it doesn't, create it with a single unread message.
                    Chat otherChat = new Chat(
                            ChatMessagesViewModel.this.chat.getId(),
                            ChatMessagesViewModel.this.userId,
                            userRoleNode,
                            userFullName,
                            userPicture,
                            chatMessage.getText(),
                            chatMessage.getTime(),
                            chatMessage.getTimestamp(),
                            1
                    );

                    otherChatReference.setValue(otherChat);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
