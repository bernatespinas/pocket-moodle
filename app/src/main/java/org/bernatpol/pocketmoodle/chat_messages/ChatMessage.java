package org.bernatpol.pocketmoodle.chat_messages;

//import java.util.Optional;

import com.google.firebase.database.ServerValue;

public class ChatMessage {
    private String text;
//    private Optional<String> picture;

    // The LocalDateTime of the device, which may not be accurate.
    private String time;

    // Firebase's server timestamp, used to sort messages.
    // TODO Object... To get Firebase's timestamp you need to send a Map<String, String>,
    //  but the final value will be a Long.
    //  A ChatMessage that will be uploaded to Firebase will have a Map<String, String>,
    //  whereas a ChatMessage retrieved from Firebase will have a Long.
    private Object timestamp;

    private String sender;

    public ChatMessage() {

    }

    ChatMessage(String text, String time, String sender) {
        this.text = text;
        this.time = time;
        this.timestamp = ServerValue.TIMESTAMP;
        this.sender = sender;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTime() {
        return this.time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Object getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(Object timestamp) {
        this.timestamp = timestamp;
    }

    public String getSender() {
        return this.sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }
}
