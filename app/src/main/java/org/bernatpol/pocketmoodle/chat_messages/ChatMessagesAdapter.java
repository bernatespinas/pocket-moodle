package org.bernatpol.pocketmoodle.chat_messages;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;

import org.bernatpol.pocketmoodle.R;

import java.util.ArrayList;

class ChatMessagesAdapter extends RecyclerView.Adapter<ChatMessagesAdapter.ChatMessageViewHolder> {
    private ArrayList<ChatMessage> chatMessages;
    private String userUid;

    ChatMessagesAdapter(ArrayList<ChatMessage> chatMessages) {
        this.chatMessages = chatMessages;
        this.userUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    @NonNull
    @Override
    public ChatMessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.chat_message, parent, false)
        ;

        return new ChatMessageViewHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatMessageViewHolder holder, int position) {
        ChatMessage chatMessage = this.chatMessages.get(position);

        holder.text.setText(chatMessage.getText());
        holder.time.setText(chatMessage.getTime());

        // If that chatMessage was sent by the current user, move it to the right/end.
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) holder.parent.getLayoutParams();

        if (chatMessage.getSender().equals(this.userUid)) {
            layoutParams.gravity = Gravity.END;
//            layoutParams.setMarginEnd(20);
            holder.parent.setBackgroundResource(R.drawable.chat_message_right);
        } else {
            layoutParams.gravity = Gravity.START;
//            layoutParams.setMarginStart(20);
            holder.parent.setBackgroundResource(R.drawable.chat_message_left);
        }

//        holder.parent.setLayoutParams(layoutParams);
    }

    @Override
    public int getItemCount() {
        return this.chatMessages.size();
    }

    void addItem(ChatMessage chatMessage) {
        this.chatMessages.add(chatMessage);
//        this.notifyItemInserted();
    }

    static class ChatMessageViewHolder extends RecyclerView.ViewHolder {
        private ConstraintLayout parent;

        private TextView text;
        private TextView time;

        ChatMessageViewHolder(@NonNull View itemView) {
            super(itemView);

            this.parent = itemView.findViewById(R.id.parent);

            this.text = itemView.findViewById(R.id.text);
            this.time = itemView.findViewById(R.id.time);
        }
    }
}
