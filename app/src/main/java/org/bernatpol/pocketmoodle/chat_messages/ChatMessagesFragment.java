package org.bernatpol.pocketmoodle.chat_messages;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;

import org.bernatpol.pocketmoodle.R;
import org.bernatpol.pocketmoodle.chat.Chat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class ChatMessagesFragment extends Fragment implements View.OnClickListener {
    private ChatMessagesViewModel chatMessagesViewModel;

    private int lastProcessedMessage;

    private RecyclerView messageList;
    private ChatMessagesAdapter adapter;
    private EditText text;
    private Spinner spinner;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();
        Chat chat = (Chat) bundle.getSerializable("chat");

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.getContext());
        String userFullName = sharedPreferences.getString("userFullName", "Unknown");
        String userPicture = sharedPreferences.getString("userPicture", null);
        String userRoleNode = sharedPreferences.getString("userRoleNode", "students");

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(chat.getOtherFullName());

        this.chatMessagesViewModel = new ViewModelProvider(
                this,
                new ChatMessagesViewModelFactory(chat, userFullName, userPicture, userRoleNode)
        ).get(ChatMessagesViewModel.class);

        this.lastProcessedMessage = 0;

        final View root = inflater.inflate(R.layout.fragment_chat_messages, container, false);

        ChatMessagesFragment.this.messageList = root.findViewById(R.id.message_list);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(
                ChatMessagesFragment.this.getContext()
        );
        layoutManager.setStackFromEnd(true);

        ChatMessagesFragment.this.messageList.setLayoutManager(layoutManager);

        ChatMessagesFragment.this.adapter = new ChatMessagesAdapter(new ArrayList<ChatMessage>());

        ChatMessagesFragment.this.messageList.setAdapter(
                ChatMessagesFragment.this.adapter
        );

        this.text = root.findViewById(R.id.text);

        this.spinner = root.findViewById(R.id.spinner2);

        root.findViewById(R.id.emojiButton).setOnClickListener(this);

        root.findViewById(R.id.send).setOnClickListener(this);

        this.chatMessagesViewModel.getMessages().observe(
                getViewLifecycleOwner(),
                new Observer<ArrayList<ChatMessage>>() {
                    @Override
                    public void onChanged(ArrayList<ChatMessage> chatMessages) {
                        int size = chatMessages.size();

                        if (size == 0) {
                            return;
                        }

                        // Add new ChatMessages to the adapter.
                        for (
                                int i = ChatMessagesFragment.this.lastProcessedMessage;
                                i < size;
                                i += 1
                        ) {
                            ChatMessagesFragment.this.adapter.addItem(chatMessages.get(i));
                        }
                        ChatMessagesFragment.this.adapter.notifyDataSetChanged();

                        // Not size-1 but size
                        ChatMessagesFragment.this.lastProcessedMessage = size;

                        ChatMessagesFragment.this.messageList.scrollToPosition(
                                ChatMessagesFragment.this.adapter.getItemCount() - 1
                        );
                    }
                }
        );

        return root;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.send) {
            String text = this.text.getText().toString();

            if (!text.isEmpty()) {

                String time = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM HH:mm"));

                ChatMessage chatMessage = new ChatMessage(text, time, FirebaseAuth.getInstance().getCurrentUser().getUid());

                this.chatMessagesViewModel.sendMessage(chatMessage);

                this.text.setText("");
            }
        }

        if (id == R.id.emojiButton) {
            spinner.setSelection(0, false);
            spinner.performClick();
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView adapter, View v, int i, long lng) {
                    String selectedItem = adapter.getItemAtPosition(i).toString();
                    text.setText(text.getText().append(selectedItem));
                    text.setSelection(text.length());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {

                }
            });
        }
    }
}
