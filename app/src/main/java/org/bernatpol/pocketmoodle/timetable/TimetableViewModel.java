package org.bernatpol.pocketmoodle.timetable;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

public class TimetableViewModel extends ViewModel {
    private MutableLiveData<ArrayList<HashMap<Integer, TimetableSubject>>> days;
    private MutableLiveData<HashMap<Integer, String>> hours;

    public TimetableViewModel() {
        this.days = new MutableLiveData<>();
        this.hours = new MutableLiveData<>();

        FirebaseDatabase.getInstance()
                .getReference("2wiam/timetable2")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        ArrayList<HashMap<Integer, TimetableSubject>> days = new ArrayList<>();

                        String[] weekDays = {"monday", "tuesday", "wednesday", "thursday", "friday"};
                        for (String week_day : weekDays) {
                            HashMap<Integer, TimetableSubject> timetableSubjects =
                                    TimetableViewModel.this.getDaySubjects(dataSnapshot, week_day)
                            ;
                            days.add(timetableSubjects);
                        }

                        TimetableViewModel.this.days.setValue(days);

                        HashMap<Integer, String> hours = new HashMap<>();

                        for (DataSnapshot child : dataSnapshot.child("hours").getChildren()) {
                            hours.put(
                                    Integer.valueOf(child.getKey()),
                                    child.getValue(String.class)
                            );
                        }

                        TimetableViewModel.this.hours.setValue(hours);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                })
        ;
    }

    private HashMap<Integer, TimetableSubject> getDaySubjects(DataSnapshot dataSnapshot, String day) {
        HashMap<Integer, TimetableSubject> daySubjects = new HashMap<>();

        Optional<TimetableSubject> currentSubject = Optional.empty();
        int hourRange = 0;

        for (DataSnapshot child : dataSnapshot.child("days/" + day).getChildren()) {
            String subjectId = child.getValue(String.class);

            if (currentSubject.isPresent()) {
                if (currentSubject.get().getSubject().equals(subjectId)) {
                    currentSubject.get().addOneLength();

                } else {
                    daySubjects.put(
                            hourRange,
                            currentSubject.get()
                    );

                    currentSubject = Optional.of(new TimetableSubject(
                            subjectId,
                            1
                    ));
                }
            } else {
                currentSubject = Optional.of(new TimetableSubject(
                        subjectId,
                        1
                ));
            }

            hourRange += 1;
        }

        // Add last TimetableSubject.
        if (currentSubject.isPresent()) {
            daySubjects.put(
                    hourRange,
                    currentSubject.get()
            );
        }

        return daySubjects;
    }

    LiveData<ArrayList<HashMap<Integer, TimetableSubject>>> getDays() {
        return this.days;
    }

    LiveData<HashMap<Integer, String>> getHours() {
        return this.hours;
    }
}