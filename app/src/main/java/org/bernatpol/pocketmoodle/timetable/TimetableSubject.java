package org.bernatpol.pocketmoodle.timetable;

public class TimetableSubject {
    private String subject;
    private int length;

    public TimetableSubject() {

    }

    public TimetableSubject(String subject, int length) {
        this.subject = subject;
        this.length = length;
    }

    public String getSubject() {
        return this.subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public int getLength() {
        return this.length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    void addOneLength() {
        this.length += 1;
    }
}
