package org.bernatpol.pocketmoodle.timetable;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import org.bernatpol.pocketmoodle.R;
import org.bernatpol.pocketmoodle.subject.Subject;
import org.bernatpol.pocketmoodle.subject.SubjectsViewModel;

import java.util.ArrayList;
import java.util.HashMap;

public class TimetableFragment extends Fragment {

    private TimetableViewModel timetableViewModel;

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        this.timetableViewModel = new ViewModelProvider(this)
                .get(TimetableViewModel.class)
        ;

        // Get the already available SubjectsViewModel created in MainActivity.
        final SubjectsViewModel subjectsViewModel = new ViewModelProvider(
                requireActivity()
        ).get(SubjectsViewModel.class);

        final View root = inflater.inflate(R.layout.fragment_timetable, container, false);

        final String[] weekDays = {
                getResources().getString(R.string.monday),
                getResources().getString(R.string.tuesday),
                getResources().getString(R.string.wednesday),
                getResources().getString(R.string.thursday),
                getResources().getString(R.string.friday),
        };

        subjectsViewModel.getSubjects().observe(getViewLifecycleOwner(), new Observer<HashMap<String, Subject>>() {
            @Override
            public void onChanged(final HashMap<String, Subject> subjects) {
                TimetableFragment.this.timetableViewModel.getDays().observe(getViewLifecycleOwner(), new Observer<ArrayList<HashMap<Integer, TimetableSubject>>>() {
                    @Override
                    public void onChanged(ArrayList<HashMap<Integer, TimetableSubject>> hashMaps) {
                        LinearLayout parent = root.findViewById(R.id.parent);

                        for (int i = 0, size = hashMaps.size(); i < size; i += 1) {
                            // +1 Because the first one, at 0, is R.id.hours.
                            LinearLayout day = (LinearLayout) parent.getChildAt(i + 1);

                            // Adding week days manually here, because if they're <include>d in
                            // R.layout.timetable_day_column they are displayed at full height until
                            // subjects are fully loaded. Toggling visibility could be another workaround.
//                    ((TextView) day.findViewById(R.id.timetable_week_day).findViewById(R.id.week_day))
//                            .setText(weekDays[i].substring(0, 3))
//                    ;
                            View weekDay = inflater.inflate(R.layout.timetable_week_day, day, false);
                            ((TextView) weekDay.findViewById(R.id.week_day)).setText(
                                    weekDays[i].substring(0, 3)
                            );

                            weekDay.setLayoutParams(new LinearLayout.LayoutParams(
                                    ViewGroup.LayoutParams.MATCH_PARENT,
                                    0,
                                    0.5f
                            ));
                            day.addView(weekDay);

                            for (final TimetableSubject timetableSubject : hashMaps.get(i).values()) {
                                View view = inflater.inflate(R.layout.timetable_subject, day, false);

                                ((TextView) view.findViewById(R.id.subject_id)).setText(
                                        timetableSubject.getSubject().toUpperCase()
                                );

                                if (((TextView) view.findViewById(R.id.subject_id)).getText().length() > 3) {
                                    ((TextView) view.findViewById(R.id.subject_id)).setTextSize(12);
                                    view.findViewById(R.id.container).setBackgroundColor(getResources().getColor(R.color.tutoria));
                                    view.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Toast.makeText(TimetableFragment.this.getContext(), "TUTORIA", Toast.LENGTH_SHORT).show();                                }
                                    });
                                } else {
                                    view.findViewById(R.id.container).setBackgroundColor(Color.parseColor(
                                            subjects.get(timetableSubject.getSubject()).getColor()
                                    ));
                                    view.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Subject subject = subjects.get(timetableSubject.getSubject());

                                            boolean subjectAvailable = subject != null && (subject.isAvailableForCurrentUser() && subject.isVisible());

                                            if (subjectAvailable) {
                                                Bundle bundle = new Bundle();
                                                bundle.putString("subjectId", timetableSubject.getSubject());

                                                NavHostFragment.findNavController(TimetableFragment.this)
                                                        .navigate(R.id.action_nav_timetable_to_nav_subject_details, bundle)
                                                ;

//                                                ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor(timetableSubject.getColor())));
                                            } else {
                                                Toast.makeText(TimetableFragment.this.getContext(), "Subject not available", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                                }

                                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                        ViewGroup.LayoutParams.MATCH_PARENT,
                                        0,
                                        timetableSubject.getLength()
                                );

                                view.setLayoutParams(layoutParams);

                                day.addView(view);
                            }
                        }
                    }
                });
            }
        });

        this.timetableViewModel.getHours().observe(getViewLifecycleOwner(), new Observer<HashMap<Integer, String>>() {
            @Override
            public void onChanged(HashMap<Integer, String> integerStringHashMap) {
                LinearLayout parent = root.findViewById(R.id.parent);

                LinearLayout hours = (LinearLayout) parent.getChildAt(0);

                for (String hour : integerStringHashMap.values()) {
                    View timetableHour = LayoutInflater
                            .from(TimetableFragment.this.getContext())
                            .inflate(R.layout.timetable_hour, hours, false);

                    // TODO Is that okay?
                    String[] beginEnd = hour.split(" - ");

                    ((TextView) timetableHour.findViewById(R.id.hour_begin)).setText(beginEnd[0]);
                    ((TextView) timetableHour.findViewById(R.id.hour_end)).setText(beginEnd[1]);

                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            0,
                            1
                    );

                    timetableHour.setLayoutParams(layoutParams);

                    hours.addView(timetableHour);
                }

                // Set the maximum amount of hours plus the week day for each column.
                for (int i = 0, count = parent.getChildCount(); i < count; i += 1) {
                    ((LinearLayout) parent.getChildAt(i)).setWeightSum(integerStringHashMap.size()+0.5f);
                }
            }
        });

        return root;
    }
}
