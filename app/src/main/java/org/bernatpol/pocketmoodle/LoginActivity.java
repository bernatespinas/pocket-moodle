package org.bernatpol.pocketmoodle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private FirebaseAuth firebaseAuth;

    private boolean isStudent;
    private LinearLayout student;
    private LinearLayout teacher;

    private EditText username;
    private EditText password;

    private Button login;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        this.firebaseAuth = FirebaseAuth.getInstance();
        this.isStudent = true;

        this.username = this.findViewById(R.id.username);
        this.password = this.findViewById(R.id.password);

        this.student = this.findViewById(R.id.student);
        this.student.setOnClickListener(this);

        this.teacher = this.findViewById(R.id.teacher);
        this.teacher.setOnClickListener(this);

        this.login = this.findViewById(R.id.login);
        this.login.setOnClickListener(this);

        this.progressBar = this.findViewById(R.id.progress_bar);

        this.getSupportActionBar().setTitle(Html.fromHtml("<font color='#FFFFFF'>"  + getString(R.string.app_name) + " </font>"));

    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser firebaseUser = this.firebaseAuth.getCurrentUser();
        if (firebaseUser != null) {
            this.startMainActivity();
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.login) {
            this.checkUserDataAndLogin();

        } else if (id == R.id.student) {
            this.isStudent = true;
            this.student.setBackgroundResource(R.drawable.selected_role_background);
            this.teacher.setBackground(null);

        } else if (id == R.id.teacher) {
            this.isStudent = false;
            this.teacher.setBackgroundResource(R.drawable.selected_role_background);
            this.student.setBackground(null);
        }
    }

    private void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        this.startActivity(intent);
        this.finish();
    }

    private void checkUserDataAndLogin() {
        boolean validFields = true;

        String username = this.username.getText().toString();
        if (username.isEmpty()) {
            this.username.setError("Enter your username");
            validFields = false;
        }

        String password = this.password.getText().toString();
        if (password.isEmpty()) {
            this.password.setError("Enter your password");
            validFields = false;
        }

        if (!validFields) {
            return;
        }

        // Mini hack para evitar tener que implantar un login de username + password, que creo que
        // se tendría que hacer a mano. De esta manera aprovechamos el de email + password ya hecho
        // y podemos identificar fácilmente si el usuario es estudiante o profesor.
        String email = username + (this.isStudent ? "@edt.alumnat.org" : "@edt.professorat.org");

        this.setInputsEnabled(false);

        this.firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            LoginActivity.this.storeUserInfoAndStartMainActivity();

                        } else {
                            Log.d("FIREBASE_AUTH", task.getException().toString());

                            new AlertDialog.Builder(LoginActivity.this)
                                    .setView(R.layout.login_error)
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .create().show()
                            ;

                            LoginActivity.this.setInputsEnabled(true);
                        }
                    }
                })
        ;
    }

    private void setInputsEnabled(boolean enabled) {
        this.login.setEnabled(enabled);

        this.student.setEnabled(enabled);
        this.teacher.setEnabled(enabled);

        this.username.setEnabled(enabled);
        this.password.setEnabled(enabled);

        this.progressBar.setVisibility(enabled ? View.INVISIBLE : View.VISIBLE);
    }

    private void storeUserInfoAndStartMainActivity() {
        final String userRoleNode = this.isStudent ? "students" : "teachers";

        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        Log.d("FIREBASE", "User UID: " + userId);

        FirebaseDatabase.getInstance()
                .getReference("/2wiam/" + userRoleNode + "/" + userId)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
                        SharedPreferences.Editor editor = sharedPreferences.edit();

                        editor.putString("userRoleNode", userRoleNode);
                        editor.putBoolean("student", LoginActivity.this.isStudent);

                        String userFullName = dataSnapshot.child("fullName").getValue(String.class);
                        Log.d("FIREBASE", "fullName: " + userFullName);
                        editor.putString("userFullName", userFullName);

                        String userPicture = dataSnapshot.child("picture").getValue(String.class);
                        Log.d("FIREBASE", "picture: " + userPicture);
                        editor.putString("userPicture", userPicture);

                        // TODO apply? Or check commit return value and that every value is non-null
                        editor.commit();

                        LoginActivity.this.startMainActivity();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }
}
