package org.bernatpol.pocketmoodle.user;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import org.bernatpol.pocketmoodle.R;

import java.util.ArrayList;

class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.UserViewHolder> {
    private ArrayList<User> users;
    private OnUserClickListener onUserClickListener;

    UsersAdapter(ArrayList<User> users, OnUserClickListener onUserClickListener) {
        this.users = users;
        this.onUserClickListener = onUserClickListener;
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.user_item, parent, false)
        ;
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        final User user = this.users.get(position);

        holder.fullName.setText(user.getFullName());

        Glide.with(this.onUserClickListener.getListenerContext())
                .load(user.getPicture())
                .circleCrop()
                .into(holder.picture)
        ;

        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UsersAdapter.this.onUserClickListener.onUserClick(user);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.users.size();
    }

    static class UserViewHolder extends RecyclerView.ViewHolder {
        private ConstraintLayout parent;

        private TextView fullName;
        private ImageView picture;

        UserViewHolder(@NonNull View itemView) {
            super(itemView);

            this.parent = itemView.findViewById(R.id.parent);

            this.fullName = itemView.findViewById(R.id.full_name);
            this.picture = itemView.findViewById(R.id.picture);
        }
    }

    public interface OnUserClickListener {
        void onUserClick(User user);
        Context getListenerContext();
    }
}
