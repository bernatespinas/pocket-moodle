package org.bernatpol.pocketmoodle.user;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;

public class UsersViewModelFactory implements ViewModelProvider.Factory {
    private Optional<String> subjectId;
    private Optional<String> topicId;
    private Optional<Boolean> students;
    // Users' IDs present here will be excluded.
    private Optional<HashSet<String>> excludedUsersIds;
    private Optional<ArrayList<String>> usersIds;

    // Fetch *students* enrolled in a particular topic of a particular subject.
    public UsersViewModelFactory(String subjectId, String topicId, HashSet<String> excludedUsersIds) {
        this.subjectId = Optional.of(subjectId);
        this.topicId = Optional.of(topicId);
        this.students = Optional.empty();
        this.excludedUsersIds = Optional.of(excludedUsersIds);
        this.usersIds = Optional.empty();
    }

    // Fetch teachers *or* students enrolled in a particular subject.
    UsersViewModelFactory(String subjectId, boolean students, HashSet<String> excludedUsersIds) {
        this.subjectId = Optional.of(subjectId);
        this.topicId = Optional.empty();
        this.students = Optional.of(students);
        this.excludedUsersIds = Optional.of(excludedUsersIds);
        this.usersIds = Optional.empty();
    }

    // Fetch *all* students *or* teachers.
    UsersViewModelFactory(boolean students, HashSet<String> excludedUsersIds) {
        this.subjectId = Optional.empty();
        this.topicId = Optional.empty();
        this.students = Optional.of(students);
        this.excludedUsersIds = Optional.of(excludedUsersIds);
        this.usersIds = Optional.empty();
    }

    // Fetch all teachers *or* students from a list of users' IDs.
    public UsersViewModelFactory(boolean students, ArrayList<String> usersIds) {
        this.subjectId = Optional.empty();
        this.topicId = Optional.empty();
        this.students = Optional.of(students);
        this.excludedUsersIds = Optional.empty();
        this.usersIds = Optional.of(usersIds);

    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (this.topicId.isPresent()) {
            return (T) new UsersViewModel(this.subjectId.get(), this.topicId.get(), this.excludedUsersIds.get());
        } else if (this.subjectId.isPresent()) {
            return (T) new UsersViewModel(this.subjectId.get(), this.students.get(), this.excludedUsersIds.get());
        } else if (this.excludedUsersIds.isPresent()) {
            return (T) new UsersViewModel(this.students.get(), this.excludedUsersIds.get());
        } else {
            return (T) new UsersViewModel(this.students.get(), this.usersIds.get());
        }
    }
}
