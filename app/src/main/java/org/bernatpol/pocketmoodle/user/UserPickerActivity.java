package org.bernatpol.pocketmoodle.user;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import org.bernatpol.pocketmoodle.R;

import java.util.ArrayList;
import java.util.HashSet;

public class UserPickerActivity extends AppCompatActivity implements UsersAdapter.OnUserClickListener {
    private UsersViewModel usersViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_picker);

        Bundle bundle = getIntent().getExtras();
        boolean students = bundle.getBoolean("students");
        ArrayList<String> excludedUsersIdsArrayList = bundle.getStringArrayList("excludedUsersIds");
        HashSet<String> excludedUsersIds = new HashSet<>();
        if (excludedUsersIdsArrayList != null) {
            excludedUsersIds.addAll(excludedUsersIdsArrayList);
        }

        String subjectId = bundle.getString("subjectId");
        if (subjectId == null) {
            // No subject specified, getting all people.
            this.usersViewModel = new ViewModelProvider(
                    this,
                    new UsersViewModelFactory(students, excludedUsersIds)
            ).get(UsersViewModel.class);

        } else {
            String topicId = bundle.getString("topicId");
            if (topicId == null) {
                // No topic specified, getting all people enrolled in this subject.
                this.usersViewModel = new ViewModelProvider(
                        this,
                        new UsersViewModelFactory(subjectId, students, excludedUsersIds)
                ).get(UsersViewModel.class);

            } else {
                // Getting all people enrolled in this topic.
                this.usersViewModel = new ViewModelProvider(
                        this,
                        new UsersViewModelFactory(subjectId, topicId, excludedUsersIds)
                ).get(UsersViewModel.class);
            }
        }

        final RecyclerView userList = UserPickerActivity.this.findViewById(R.id.user_list);
        userList.addItemDecoration(
                new DividerItemDecoration(userList.getContext(), DividerItemDecoration.VERTICAL)
        );

        this.usersViewModel.getPeople().observeForever(new Observer<ArrayList<User>>() {
            @Override
            public void onChanged(ArrayList<User> users) {
                TextView status = UserPickerActivity.this.findViewById(R.id.status);

                if (users.isEmpty()) {
                    status.setText(R.string.nothing_here);

                } else {
                    userList.setLayoutManager(new LinearLayoutManager(UserPickerActivity.this));

                    userList.setAdapter(new UsersAdapter(users, UserPickerActivity.this));

                    status.setVisibility(View.GONE);
                    userList.setVisibility(View.VISIBLE);
                }
            }
        });

        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setDisplayShowHomeEnabled(true);
        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_24dp);
        this.getSupportActionBar().setTitle(Html.fromHtml("<font color='#FFFFFF'>"  + getString(R.string.user_picker) + " </font>"));
    }

    @Override
    public void onUserClick(User user) {
        Intent result = new Intent();
        result.putExtra("user", user);

        this.setResult(RESULT_OK, result);
        this.finish();
    }

    @Override
    public Context getListenerContext() {
        return this.getApplicationContext();
    }

    @Override public boolean onSupportNavigateUp() { onBackPressed(); return true; }
}
