package org.bernatpol.pocketmoodle.user;

import java.io.Serializable;

public class User implements Serializable {
    private String id;
    private String fullName;
    private String picture;
    private String roleNode;

    public User() {

    }

    public User(String id, String fullName, String picture, String roleNode) {
        this.id = id;
        this.fullName = fullName;
        this.picture = picture;
        this.roleNode = roleNode;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return this.fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPicture() {
        return this.picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getRoleNode() {
        return this.roleNode;
    }

    public void setRoleNode(String roleNode) {
        this.roleNode = roleNode;
    }
}
