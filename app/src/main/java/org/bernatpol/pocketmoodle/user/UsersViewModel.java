package org.bernatpol.pocketmoodle.user;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashSet;

public class UsersViewModel extends ViewModel {
    private MutableLiveData<ArrayList<User>> people;

    //Pasar lista, sino se ha pasado lista, coje alumnos de la UF actual.
    // Get all *students* enrolled in a particular topic from a particular subject.
    public UsersViewModel(String subjectId, String topicId, final HashSet<String> excludedUsersIds) {
        this.people = new MutableLiveData<>();

        FirebaseDatabase.getInstance()
                .getReference(String.format("2wiam/subjects/%s/topics/%s/students", subjectId.toLowerCase(), topicId.toLowerCase()))
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        final ArrayList<String> studentsIds = UsersViewModel.this.fetchUserIds(
                                dataSnapshot,
                                excludedUsersIds
                        );

                        UsersViewModel.this.fetchStudents(studentsIds);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                })
        ;
    }

    //Botones Students o Teachers de una asignatura
    // Get all teachers *or* students enrolled in a particular subject.
    UsersViewModel(String subjectId, boolean students, final HashSet<String> excludedUsersIds) {
        this.people = new MutableLiveData<>();

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("2wiam/subjects/" + subjectId.toLowerCase());

        if (students) {
            reference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    HashSet<String> studentsIds = new HashSet<>();

                    for (DataSnapshot topicSnapshot : dataSnapshot.child("topics").getChildren()) {
                        studentsIds.addAll(UsersViewModel.this.fetchUserIds(
                                topicSnapshot.child("students"),
                                excludedUsersIds
                        ));
                    }

                    UsersViewModel.this.fetchStudents(new ArrayList<>(studentsIds));
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        } else {
            reference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    ArrayList<String> teachersId = UsersViewModel.this.fetchUserIds(
                            dataSnapshot.child("teachers"),
                            excludedUsersIds
                    );

                    UsersViewModel.this.fetchTeachers(teachersId);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    //Chats
    // Get all teachers *or* students.
    UsersViewModel(boolean students, final HashSet<String> excludedUsersIds) {
        this.people = new MutableLiveData<>();

        final String usersRole = students ? "students" : "teachers";

        FirebaseDatabase.getInstance()
                .getReference("2wiam/" + usersRole)
                .orderByChild("fullName")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        ArrayList<User> students = new ArrayList<>();

                        String currentUserId = FirebaseAuth.getInstance().getCurrentUser().getUid();

                        for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                            String userId = userSnapshot.getKey();

                            if (!currentUserId.equals(userId) && !excludedUsersIds.contains(userId)) {
                                students.add(new User(
                                        userId,
                                        userSnapshot.child("fullName").getValue(String.class),
                                        userSnapshot.child("picture").getValue(String.class),
                                        usersRole
                                ));
                            }
                        }

                        UsersViewModel.this.people.setValue(students);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                })
        ;
    }

    //Pasar lista, ya se ha pasado lista y solo mira foto y nombre de los alumnos.
    UsersViewModel(boolean students, ArrayList<String> usersIds) {
        this.people = new MutableLiveData<>();

        String roleNode = students ? "students" : "teachers";
        this.fetchUsers(usersIds, roleNode);
    }

    private ArrayList<String> fetchUserIds(DataSnapshot dataSnapshot, HashSet<String> excludedUsersIds) {
        String currentUserId = FirebaseAuth.getInstance().getCurrentUser().getUid();

        ArrayList<String> usersId = new ArrayList<>();

        for (DataSnapshot userIdSnapshot : dataSnapshot.getChildren()) {
            String userId = userIdSnapshot.getKey();
            if (!currentUserId.equals(userId) && !excludedUsersIds.contains(userId)) {
                usersId.add(userId);
            }
        }

        return usersId;
    }

    private void fetchStudents(ArrayList<String> studentsIds) {
        this.fetchUsers(studentsIds, "students");
    }

    private void fetchTeachers(ArrayList<String> teachersIds) {
        this.fetchUsers(teachersIds, "teachers");
    }

    private void fetchUsers(final ArrayList<String> usersIds, final String roleNode) {
        FirebaseDatabase.getInstance()
                .getReference("2wiam/" + roleNode)
                .orderByChild("fullName")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        ArrayList<User> students = new ArrayList<>();

                        for (String studentId : usersIds) {
                            String fullName = dataSnapshot
                                    .child(studentId + "/fullName")
                                    .getValue(String.class)
                                    ;
                            String picture = dataSnapshot
                                    .child(studentId + "/picture")
                                    .getValue(String.class)
                                    ;

                            students.add(new User(studentId, fullName, picture, roleNode));
                        }

                        UsersViewModel.this.people.setValue(students);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                })
        ;
    }

    public LiveData<ArrayList<User>> getPeople() {
        return this.people;
    }
}
