package org.bernatpol.pocketmoodle.task;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;

public class Task implements Serializable, Parcelable {
    private static final DateTimeFormatter TASK_DATE_END_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yy HH:mm");

    public final static Comparator<Task> TASK_DATE_END_COMPARATOR = new Comparator<Task>() {
        @Override
        public int compare(Task o1, Task o2) {
            return LocalDateTime.parse(o1.dateEnd, Task.TASK_DATE_END_FORMATTER).compareTo(
                    LocalDateTime.parse(o2.dateEnd, Task.TASK_DATE_END_FORMATTER)
            );
        }
    };

    private String id;
    private String subjectId;
    private String topicId;

    private String title;
    private String description;
    private String dateEnd;
    private boolean visible;

    private String typeNode;

    public Task() {

    }

    protected Task(Parcel in) {
        this.id = in.readString();
        this.subjectId = in.readString();
        this.topicId = in.readString();

        this.title = in.readString();
        this.description = in.readString();
        this.dateEnd = in.readString();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            this.visible = in.readBoolean();
        } else {
            this.visible = Boolean.parseBoolean(in.readString());
        }

        this.typeNode = in.readString();
    }

    public static final Creator<Task> CREATOR = new Creator<Task>() {
        @Override
        public Task createFromParcel(Parcel in) {
            return new Task(in);
        }

        @Override
        public Task[] newArray(int size) {
            return new Task[size];
        }
    };

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubjectId() {
        return this.subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getTopicId() {
        return this.topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateEnd() {
        return this.dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public boolean isVisible() {
        return this.visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getTypeNode() {
        return this.typeNode;
    }

    public void setTypeNode(String typeNode) {
        this.typeNode = typeNode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.subjectId);
        dest.writeString(this.topicId);

        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeString(this.dateEnd);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            dest.writeBoolean(this.visible);
        } else {
            dest.writeString(Boolean.toString(this.visible));
        }

        dest.writeString(this.typeNode);
    }
}
