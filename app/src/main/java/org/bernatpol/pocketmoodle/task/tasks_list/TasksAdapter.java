package org.bernatpol.pocketmoodle.task.tasks_list;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import org.bernatpol.pocketmoodle.R;
import org.bernatpol.pocketmoodle.task.Task;

import java.util.ArrayList;

class TasksAdapter extends RecyclerView.Adapter<TasksAdapter.TaskViewHolder> {
    private ArrayList<Task> tasks;
    private OnTaskClickListener onTaskClickListener;

    TasksAdapter(ArrayList<Task> tasks, OnTaskClickListener onTaskClickListener) {
        this.tasks = tasks;
        this.onTaskClickListener = onTaskClickListener;
    }

    @NonNull
    @Override
    public TaskViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.task_item, parent, false)
        ;
        return new TaskViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TaskViewHolder holder, int position) {
        final Task task = this.tasks.get(position);

        holder.title.setText(task.getTitle());
        holder.subjectTopicIds.setText(String.format(
                "%s - %s", task.getSubjectId().toUpperCase(), task.getTopicId().toUpperCase()
        ));
        holder.dateEnd.setText(task.getDateEnd());

        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TasksAdapter.this.onTaskClickListener.onTaskClick(task);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.tasks.size();
    }

    static class TaskViewHolder extends RecyclerView.ViewHolder {
        private ConstraintLayout parent;

        private TextView title;
        private TextView subjectTopicIds;
        private TextView dateEnd;

        TaskViewHolder(@NonNull View itemView) {
            super(itemView);

            this.parent = itemView.findViewById(R.id.parent);

            this.title = itemView.findViewById(R.id.title);
            this.subjectTopicIds = itemView.findViewById(R.id.subject_topic_ids);
            this.dateEnd = itemView.findViewById(R.id.date_end);
        }
    }

    public interface OnTaskClickListener {
        void onTaskClick(Task task);
    }
}
