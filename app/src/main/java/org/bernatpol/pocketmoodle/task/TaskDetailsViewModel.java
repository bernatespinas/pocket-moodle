package org.bernatpol.pocketmoodle.task;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Optional;

class TaskDetailsViewModel extends ViewModel {
    private MutableLiveData<ArrayList<Upload>> taskAttachedUploads;

    private MutableLiveData<Optional<TaskDetailsStudent>> taskDetailsStudent;
    private MutableLiveData<Optional<TaskDetailsTeacher>> taskDetailsTeacher;

    // Fetch info about a particular student's submission.
    TaskDetailsViewModel(Task task, String userId) {
        this.fetchTaskAttachedUploads(task);

        this.taskDetailsStudent = new MutableLiveData<>();

        FirebaseDatabase.getInstance().getReference(String.format(
                "2wiam/uploads/%s/%s/%s/%s/%s", userId, task.getSubjectId().toLowerCase(), task.getTopicId().toLowerCase(), task.getTypeNode(), task.getId())
        ).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        Double grading = dataSnapshot.child("grading").getValue(Double.class);

                        ArrayList<Upload> uploads = new ArrayList<>();
                        for (DataSnapshot uploadSnapshot : dataSnapshot.child("uploads").getChildren()) {
                            uploads.add(uploadSnapshot.getValue(Upload.class));
                        }

                        TaskDetailsViewModel.this.taskDetailsStudent.setValue(
                                Optional.of(new TaskDetailsStudent(
                                        grading == null ? Optional.<Double>empty() : Optional.of(grading),
                                        uploads
                                ))
                        );
                    } else {
                        TaskDetailsViewModel.this.taskDetailsStudent.setValue(
                                Optional.<TaskDetailsStudent>empty()
                        );
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            })
        ;
    }

    // Fetch info for the teacher (currently nothing special).
    TaskDetailsViewModel(Task task) {
        this.fetchTaskAttachedUploads(task);
    }

    private void fetchTaskAttachedUploads(Task task) {
        this.taskAttachedUploads = new MutableLiveData<>();

        FirebaseDatabase.getInstance().getReference(String.format(
                "2wiam/topicItems/%s/%s/%s/%s/files", task.getSubjectId().toLowerCase(), task.getTopicId().toLowerCase(), task.getTypeNode(), task.getId()
        )).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot filesSnapshot) {
                ArrayList<Upload> _taskAttachedUploads = new ArrayList<>();

                for (DataSnapshot uploadSnapshot : filesSnapshot.getChildren()) {
                    _taskAttachedUploads.add(uploadSnapshot.getValue(Upload.class));
                }

                TaskDetailsViewModel.this.taskAttachedUploads.setValue(_taskAttachedUploads);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    LiveData<ArrayList<Upload>> getTaskAttachedUploads() {
        return this.taskAttachedUploads;
    }

    LiveData<Optional<TaskDetailsStudent>> getTaskDetailsStudent() {
        return this.taskDetailsStudent;
    }

    static class TaskDetailsStudent {
        private Optional<Double> grading;
        private ArrayList<Upload> uploads;

        TaskDetailsStudent(Optional<Double> grading, ArrayList<Upload> uploads) {
            this.grading = grading;
            this.uploads = uploads;
        }

        public Optional<Double> getGrading() {
            return this.grading;
        }

        public ArrayList<Upload> getUploads() {
            return this.uploads;
        }
    }

    class TaskDetailsTeacher {

    }
}
