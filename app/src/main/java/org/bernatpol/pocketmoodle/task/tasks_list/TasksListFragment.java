package org.bernatpol.pocketmoodle.task.tasks_list;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.bernatpol.pocketmoodle.R;
import org.bernatpol.pocketmoodle.task.Task;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class TasksListFragment extends Fragment implements TasksAdapter.OnTaskClickListener {

    public TasksListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root =  inflater.inflate(R.layout.fragment_tasks_list, container, false);

        Bundle bundle = this.getArguments();

        String title = bundle.getString("title");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);

        ArrayList<Task> tasks = bundle.getParcelableArrayList("tasks");

        RecyclerView taskList = root.findViewById(R.id.task_list);
        taskList.addItemDecoration(
                new DividerItemDecoration(taskList.getContext(), DividerItemDecoration.VERTICAL)
        );

        taskList.setLayoutManager(
                new LinearLayoutManager(this.getContext())
        );

        taskList.setAdapter(
                new TasksAdapter(tasks, this)
        );

        return root;
    }

    @Override
    public void onTaskClick(Task task) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("task", task);

        NavHostFragment.findNavController(this)
                .navigate(R.id.action_nav_tasks_list_to_nav_task, bundle)
        ;
    }
}
