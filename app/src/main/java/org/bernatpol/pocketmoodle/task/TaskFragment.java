package org.bernatpol.pocketmoodle.task;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import org.bernatpol.pocketmoodle.R;

import java.util.ArrayList;
import java.util.Optional;

/**
 * A simple {@link Fragment} subclass.
 */
public class TaskFragment extends Fragment {
    private TaskDetailsViewModel taskDetailsViewModel;

    public TaskFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();
        Task task = (Task) bundle.getSerializable("task");

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(task.getTitle());

        // Inflate the layout for this fragment
        final View root = inflater.inflate(R.layout.fragment_task, container, false);

        ((TextView) root.findViewById(R.id.title))
                .setText(task.getTitle())
        ;
        ((TextView) root.findViewById(R.id.description))
                .setText(task.getDescription())
        ;

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.getContext());
        boolean student = sharedPreferences.getBoolean("student", true);

        if (student) {
            this.taskDetailsViewModel = new ViewModelProvider(
                    this,
                    new TaskDetailsViewModelFactory(task, FirebaseAuth.getInstance().getCurrentUser().getUid())
            ).get(TaskDetailsViewModel.class);

            View studentInfo = inflater.inflate(R.layout.fragment_task_student_info, container, false);

            this.addStudentInfo(studentInfo, task, inflater, container);

            ((LinearLayout) root.findViewById(R.id.parent))
                    .addView(studentInfo)
            ;
        } else {
            this.taskDetailsViewModel = new ViewModelProvider(
                    this,
                    new TaskDetailsViewModelFactory(task)
            ).get(TaskDetailsViewModel.class);

            View teacherInfo = inflater.inflate(R.layout.fragment_task_teacher_info, container, false);

            this.addTeacherInfo(teacherInfo, task);

            ((LinearLayout) root.findViewById(R.id.parent))
                    .addView(teacherInfo)
            ;
        }

        this.taskDetailsViewModel.getTaskAttachedUploads().observe(getViewLifecycleOwner(), new Observer<ArrayList<Upload>>() {
            @Override
            public void onChanged(ArrayList<Upload> uploads) {
                TextView notesStatus = root.findViewById(R.id.files_status);
                LinearLayout notes = root.findViewById(R.id.files_list);
                TaskFragment.this.populateUploadList(uploads, notesStatus, notes, inflater, container);
            }
        });

        return root;
    }

    private void populateUploadList(ArrayList<Upload> uploads, TextView status, LinearLayout list, LayoutInflater inflater, ViewGroup container) {
//        TextView filesStatus = root.findViewById(R.id.files_status);

        if (uploads.isEmpty()) {
            status.setText(R.string.nothing_here);
            return;
        }

        for (final Upload upload : uploads) {
            View uploadItem = inflater.inflate(R.layout.upload_item, container, false);

            ((TextView) uploadItem.findViewById(R.id.name))
                    .setText(upload.getName())
            ;
            uploadItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(TaskFragment.this.getContext(), upload.getLink(), Toast.LENGTH_LONG).show();
                }
            });

            list.addView(uploadItem);
        }

        status.setVisibility(View.GONE);
        list.setVisibility(View.VISIBLE);
    }

    private void addStudentInfo(final View studentInfo, final Task task, final LayoutInflater inflater, final ViewGroup container) {
        this.taskDetailsViewModel.getTaskDetailsStudent().observe(getViewLifecycleOwner(), new Observer<Optional<TaskDetailsViewModel.TaskDetailsStudent>>() {
            @Override
            public void onChanged(Optional<TaskDetailsViewModel.TaskDetailsStudent> taskDetailsStudent) {
                ((TextView) studentInfo.findViewById(R.id.due_date_status))
                        .setText(task.getDateEnd())
                ;

                TextView submittedStatus = studentInfo.findViewById(R.id.submitted_status);
                TextView gradingStatus = studentInfo.findViewById(R.id.grading_status);
                TextView uploadsStatus = studentInfo.findViewById(R.id.uploads_status);

                if (taskDetailsStudent.isPresent()) {
                    submittedStatus.setText(R.string.submitted);

                    if (taskDetailsStudent.get().getGrading().isPresent()) {
                        gradingStatus.setText(String.format("%.2f", taskDetailsStudent.get().getGrading().get()));
                    } else {
                        gradingStatus.setText(R.string.not_graded);
                    }

                    TaskFragment.this.populateUploadList(
                            taskDetailsStudent.get().getUploads(),
                            uploadsStatus,
                            (LinearLayout) studentInfo.findViewById(R.id.uploads_list),
                            inflater,
                            container
                    );
//                    for (final Upload upload : taskDetailsStudent.get().getUploads()) {
//                        View uploadItem = inflater.inflate(R.layout.upload_item, container, false);
//                        ((TextView) uploadItem.findViewById(R.id.name))
//                                .setText(upload.getName())
//                        ;
//                        uploadItem.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                Toast.makeText(TaskFragment.this.getContext(), upload.getLink(), Toast.LENGTH_SHORT).show();
//                            }
//                        });
//                    }
                } else {
                    submittedStatus.setText(R.string.not_submitted);
                    gradingStatus.setText(R.string.not_graded);
                    uploadsStatus.setText(R.string.nothing_here);
                }
            }
        });
    }

    private void addTeacherInfo(View teacherInfo, Task task) {
        ((TextView) teacherInfo.findViewById(R.id.due_date_status))
                .setText(task.getDateEnd())
        ;
    }
}
