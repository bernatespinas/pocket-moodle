package org.bernatpol.pocketmoodle.task;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import java.util.Optional;

public class TaskDetailsViewModelFactory implements ViewModelProvider.Factory {
    private Task task;
    private Optional<String> studentId;

    // Constructor to get info about the student's submission.
    TaskDetailsViewModelFactory(Task task, String studentId) {
        this.task = task;
        this.studentId = Optional.of(studentId);
    }

    // Constructor to get relevant info for the teacher.
    TaskDetailsViewModelFactory(Task task) {
        this.task = task;
        this.studentId = Optional.empty();
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (this.studentId.isPresent()) {
            return (T) new TaskDetailsViewModel(this.task, this.studentId.get());
        } else {
            return (T) new TaskDetailsViewModel(this.task);
        }
    }
}
