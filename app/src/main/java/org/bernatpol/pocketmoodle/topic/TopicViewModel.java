package org.bernatpol.pocketmoodle.topic;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.bernatpol.pocketmoodle.task.Task;
import org.bernatpol.pocketmoodle.task.Upload;

import java.util.HashMap;

class TopicViewModel extends ViewModel {

    private MutableLiveData<HashMap<String, Upload>> notes;
    private MutableLiveData<HashMap<String, Task>> tasks;
    private MutableLiveData<HashMap<String, Task>> exams;

    TopicViewModel(final String subjectId, final String topicId, final boolean student) {
        this.notes = new MutableLiveData<>();
        this.tasks = new MutableLiveData<>();
        this.exams = new MutableLiveData<>();

        FirebaseDatabase.getInstance()
                .getReference("2wiam/topicItems/" + subjectId + "/" + topicId)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        HashMap<String, Upload> notes = new HashMap<>();

                        for (DataSnapshot child : dataSnapshot.child("notes").getChildren()) {
                            notes.put(child.getKey(), child.getValue(Upload.class));
                        }

                        TopicViewModel.this.notes.setValue(notes);


                        HashMap<String, Task> tasks = new HashMap<>();

                        for (DataSnapshot child : dataSnapshot.child("tasks").getChildren()) {
                            Task task = child.getValue(Task.class);
                            if (!student || task.isVisible()) {
                                task.setId(child.getKey());
                                task.setSubjectId(subjectId);
                                task.setTopicId(topicId);
                                task.setTypeNode("tasks");
                                tasks.put(child.getKey(), task);
                            }
                        }

                        TopicViewModel.this.tasks.setValue(tasks);


                        HashMap<String, Task> exams = new HashMap<>();

                        for (DataSnapshot child : dataSnapshot.child("exams").getChildren()) {
                            Task exam = child.getValue(Task.class);
                            if (!student || exam.isVisible()) {
                                exam.setId(child.getKey());
                                exam.setSubjectId(subjectId);
                                exam.setTopicId(topicId);
                                exam.setTypeNode("exams");
                                exams.put(child.getKey(), exam);
                            }
                        }

                        TopicViewModel.this.exams.setValue(exams);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                })
        ;
    }

    LiveData<HashMap<String, Upload>> getNotes() {
        return this.notes;
    }

    LiveData<HashMap<String, Task>> getTasks() {
        return this.tasks;
    }

    LiveData<HashMap<String, Task>> getExams() {
        return this.exams;
    }
}
