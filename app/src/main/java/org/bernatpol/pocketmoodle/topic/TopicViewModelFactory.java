package org.bernatpol.pocketmoodle.topic;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

class TopicViewModelFactory implements ViewModelProvider.Factory {
    private String subjectId;
    private String topic;
    private boolean student;

    TopicViewModelFactory(String subjectId, String topic, boolean student) {
        this.subjectId = subjectId;
        this.topic = topic;
        this.student = student;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new TopicViewModel(this.subjectId, this.topic, this.student);
    }
}
