package org.bernatpol.pocketmoodle.topic;

import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.BlendMode;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.preference.PreferenceManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.bernatpol.pocketmoodle.R;
import org.bernatpol.pocketmoodle.subject.VisibilityBottomSheetDialogFragment;
import org.bernatpol.pocketmoodle.task.Task;
import org.bernatpol.pocketmoodle.task.Upload;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class TopicFragment extends Fragment {
    private TopicViewModel topicViewModel;

    private String subjectId;
    private String topicId;
    private String color;

    public TopicFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        this.subjectId = bundle.getString("subjectId");
        this.topicId = bundle.getString("topicId");
        this.color = bundle.getString("color");

        // Set Activity's/Toolbar's title to $subject - $topic.
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(
                this.subjectId.toUpperCase() + " - " + this.topicId.toUpperCase()
        );
        ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(
                new ColorDrawable(Color.parseColor(this.color))
        );


        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.getContext());
        final boolean student = sharedPreferences.getBoolean("student", true);

        this.topicViewModel = new ViewModelProvider(
                this,
                new TopicViewModelFactory(this.subjectId, this.topicId, student)
        ).get(TopicViewModel.class);

        // Inflate the layout for this fragment
        final View root = inflater.inflate(R.layout.fragment_topic, container, false);

        this.populateNotesList(root, inflater, container);
        this.populateTasksAndExamsLists(root, inflater, container, student);

        return root;
    }

    private void populateNotesList(final View root, final LayoutInflater inflater, final ViewGroup container) {
        TextView newNotesHeader = root.findViewById(R.id.new_notes_header);
        this.darkenHeader(newNotesHeader);

        final LinearLayout newNotes = root.findViewById(R.id.new_notes);
        this.lightenList(newNotes);

        this.topicViewModel.getNotes().observe(getViewLifecycleOwner(), new Observer<HashMap<String, Upload>>() {
            @Override
            public void onChanged(HashMap<String, Upload> stringTaskHashMap) {
                TextView notesStatus = root.findViewById(R.id.notes_status);

                if (stringTaskHashMap.isEmpty()) {
                    notesStatus.setText(R.string.nothing_here);
                    return;
                }

                // Clear previous tasks.
                newNotes.removeViews(1, newNotes.getChildCount()-1);

                for (final Map.Entry<String, Upload> entry : stringTaskHashMap.entrySet()) {
                    View uploadItem = inflater.inflate(R.layout.upload_item, container, false);

                    TextView uploadName =  uploadItem.findViewById(R.id.name);
                    uploadName.setText(entry.getValue().getName());
                    uploadName.setTextSize(18);
                    uploadName.setTypeface(Typeface.DEFAULT_BOLD);

                    uploadItem.findViewById(R.id.picture).setVisibility(View.GONE);

                    uploadItem.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(
                                    TopicFragment.this.getContext(),
                                    entry.getValue().getLink(),
                                    Toast.LENGTH_SHORT
                            ).show();
                        }
                    });

                    newNotes.addView(uploadItem);
                }

                notesStatus.setVisibility(View.GONE);
//                newNotes.setVisibility(View.VISIBLE);
            }
        });
    }

    private void inflateTaskItem(View taskItem, final Task task, boolean student, final String type) {
        ((TextView) taskItem.findViewById(R.id.title))
                .setText(task.getTitle())
        ;
        ((TextView) taskItem.findViewById(R.id.date_end))
                .setText(task.getDateEnd())
        ;

        if (task.isVisible()) {
            taskItem.findViewById(R.id.visible).setVisibility(View.GONE);
        }
//        taskItem.setBackgroundColor(Color.parseColor(TopicFragment.this.color));
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//            taskItem.setBackgroundTintBlendMode(BlendMode.LIGHTEN);
//        }
//        taskItem.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(TopicFragment.this.color)));

        taskItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("task", task);

                NavHostFragment.findNavController(TopicFragment.this)
                        .navigate(R.id.action_nav_topic_to_nav_task, bundle)
                ;
            }
        });

        if (!student) {
            taskItem.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    new VisibilityBottomSheetDialogFragment(
                            TopicFragment.this.subjectId,
                            TopicFragment.this.topicId,
                            type + "/" + task.getId()
                    ).show(getChildFragmentManager(), "Subject lol");
                    return true;
                }
            });
        }
    }

    private void darkenHeader(TextView header) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            header.setBackgroundTintBlendMode(BlendMode.DARKEN);
        }
        header.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(TopicFragment.this.color)));
    }

    private void lightenList(LinearLayout list) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            list.setBackgroundTintBlendMode(BlendMode.HARD_LIGHT);
        }

        list.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(TopicFragment.this.color)));
    }

    private void populateTasksAndExamsLists(final View root, final LayoutInflater inflater, final ViewGroup container, final boolean student) {
        TextView newTasksHeader = root.findViewById(R.id.new_tasks_header);
        this.darkenHeader(newTasksHeader);

        final LinearLayout newTasks = root.findViewById(R.id.new_tasks);
        this.lightenList(newTasks);

        TextView newExamsHeader = root.findViewById(R.id.new_exams_header);
        this.darkenHeader(newExamsHeader);

        final LinearLayout newExams = root.findViewById(R.id.new_exams);
        this.lightenList(newExams);

        this.topicViewModel.getTasks().observe(getViewLifecycleOwner(), new Observer<HashMap<String, Task>>() {
            @Override
            public void onChanged(HashMap<String, Task> stringTaskHashMap) {
                // Clear previous tasks.
                newTasks.removeViews(1, newTasks.getChildCount()-1);

                TextView tasksStatus = root.findViewById(R.id.tasks_status);

                if (stringTaskHashMap.isEmpty()) {
                    tasksStatus.setText(R.string.nothing_here);
                    tasksStatus.setVisibility(View.VISIBLE);

                    return;
                }

                for (Map.Entry<String, Task> entry : stringTaskHashMap.entrySet()) {
                    View taskItem = inflater.inflate(R.layout.topic_date_item, container, false);
                    final Task task = entry.getValue();

                    TopicFragment.this.inflateTaskItem(taskItem, task, student, "tasks");

                    newTasks.addView(taskItem);
                }

                tasksStatus.setVisibility(View.GONE);
            }
        });

        this.topicViewModel.getExams().observe(getViewLifecycleOwner(), new Observer<HashMap<String, Task>>() {
            @Override
            public void onChanged(HashMap<String, Task> stringTaskHashMap) {
                // Clear previous exams.
                newExams.removeViews(1, newExams.getChildCount()-1);

                TextView examsStatus = root.findViewById(R.id.exams_status);

                if (stringTaskHashMap.isEmpty()) {
                    examsStatus.setText(R.string.nothing_here);
                    examsStatus.setVisibility(View.VISIBLE);
                    return;
                }

                for (Map.Entry<String, Task> entry : stringTaskHashMap.entrySet()) {
                    View taskItem = inflater.inflate(R.layout.topic_date_item, container, false);
                    final Task exam = entry.getValue();

                    TopicFragment.this.inflateTaskItem(taskItem, exam, student, "exams");

                    newExams.addView(taskItem);
                }

                examsStatus.setVisibility(View.GONE);
            }
        });
    }
}