package org.bernatpol.pocketmoodle.topic;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;

@IgnoreExtraProperties
public class Topic implements Serializable {
    private String id;
    private String name;
    private boolean visible;

    private boolean availableForCurrentUser;

    public Topic() {

    }

    public String getId() {
        return this.id;
    }

//    @Exclude
    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isVisible() {
        return this.visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Exclude
    public boolean isAvailableForCurrentUser() {
        return this.availableForCurrentUser;
    }

    @Exclude
    public void setAvailableForCurrentUser(boolean availableForCurrentUser) {
        this.availableForCurrentUser = availableForCurrentUser;
    }

    public String getIdAndName() {
        return this.id + " - " + this.name;
    }
}
