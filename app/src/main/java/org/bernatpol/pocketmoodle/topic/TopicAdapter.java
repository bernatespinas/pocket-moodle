package org.bernatpol.pocketmoodle.topic;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import org.bernatpol.pocketmoodle.R;

import java.util.ArrayList;

public class TopicAdapter extends RecyclerView.Adapter<TopicAdapter.TopicViewHolder> {
    private ArrayList<Topic> topics;
    private OnTopicClickListener onTopicClickListener;

    public TopicAdapter(ArrayList<Topic> topics, OnTopicClickListener onTopicClickListener) {
        this.topics = topics;
        this.onTopicClickListener = onTopicClickListener;
    }

    @NonNull
    @Override
    public TopicViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.subject_topic_item, parent, false)
        ;
        return new TopicViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TopicViewHolder holder, int position) {
        final Topic topic = this.topics.get(position);

        holder.id.setText(topic.getId());
        holder.name.setText(topic.getName());

        if (!topic.isAvailableForCurrentUser()) {
            holder.status.setImageResource(R.drawable.ic_lock_24dp);
            holder.status.setVisibility(View.VISIBLE);
        } else if (!topic.isVisible()) {
            holder.status.setImageResource(R.drawable.ic_visibility_off_24dp);
            holder.status.setVisibility(View.VISIBLE);
        } else {
            holder.status.setVisibility(View.GONE);
        }

        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TopicAdapter.this.onTopicClickListener.onTopicClick(topic);
            }
        });
        holder.parent.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return TopicAdapter.this.onTopicClickListener.onTopicLongClick(topic);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.topics.size();
    }

    static class TopicViewHolder extends RecyclerView.ViewHolder {
        private ConstraintLayout parent;

        private TextView id;
        private TextView name;
        private ImageView status;

        TopicViewHolder(@NonNull View itemView) {
            super(itemView);

            this.parent = itemView.findViewById(R.id.parent);

            this.id = itemView.findViewById(R.id.id);
            this.name = itemView.findViewById(R.id.name);
            this.status = itemView.findViewById(R.id.status);
        }
    }

    public interface OnTopicClickListener {
        void onTopicClick(Topic topic);
        boolean onTopicLongClick(Topic topic);
    }
}
