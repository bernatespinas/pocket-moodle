package org.bernatpol.pocketmoodle.chat;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.bernatpol.pocketmoodle.R;

public class ChatStudentsTeachersBottomSheetDialogFragment extends BottomSheetDialogFragment implements View.OnClickListener {
    private OnUserRoleClickedListener onUserRoleClickedListener;

    ChatStudentsTeachersBottomSheetDialogFragment(OnUserRoleClickedListener onUserRoleClickedListener) {
        this.onUserRoleClickedListener = onUserRoleClickedListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.chats_students_teachers_dialog, container, false);

        root.findViewById(R.id.students).setOnClickListener(this);
        root.findViewById(R.id.teachers).setOnClickListener(this);

        return root;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.students) {
            this.onUserRoleClickedListener.onUserRoleClicked(true);
            this.dismiss();

        } else if (id == R.id.teachers) {
            this.onUserRoleClickedListener.onUserRoleClicked(false);
            this.dismiss();
        }
    }

    interface OnUserRoleClickedListener {
        void onUserRoleClicked(boolean students);
    }
}
