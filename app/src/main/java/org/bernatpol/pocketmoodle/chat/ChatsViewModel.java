package org.bernatpol.pocketmoodle.chat;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import org.bernatpol.pocketmoodle.user.User;

import java.util.ArrayList;
import java.util.Collections;

class ChatsViewModel extends ViewModel {
    private MutableLiveData<ArrayList<Chat>> chats;

    ChatsViewModel(String userRoleNode) {
        this.chats = new MutableLiveData<>();

        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();

        FirebaseDatabase.getInstance()
                .getReference("2wiam/" + userRoleNode + "/" + userId + "/chats")
                .orderByChild("timestamp")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        ArrayList<Chat> chats = new ArrayList<>();

                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            Chat chat = child.getValue(Chat.class);
                            chat.setId(child.getKey());

                            chats.add(chat);
                        }

                        Collections.reverse(chats);
                        ChatsViewModel.this.chats.setValue(chats);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                })
        ;
    }

    static Chat createChat(User otherUser) {
        return new Chat(
                FirebaseDatabase.getInstance().getReference().push().getKey(),
                otherUser.getId(),
                otherUser.getRoleNode(),
                otherUser.getFullName(),
                otherUser.getPicture(),
                // TODO
                "",
                "",
                ServerValue.TIMESTAMP,
                0
        );
    }

    LiveData<ArrayList<Chat>> getChats() {
        return this.chats;
    }
}
