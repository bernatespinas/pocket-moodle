package org.bernatpol.pocketmoodle.chat;

import com.google.firebase.database.Exclude;

import java.io.Serializable;

public class Chat implements Serializable {
    private String id;

    private String otherId;
    private String otherRoleNode;

    private String otherFullName;
    private String otherPicture;

    private String lastMessage;
    private String time;
    private Object timestamp;

    private int unreadMessages;

    public Chat() {

    }

    public Chat(
            String id,
            String otherId, String otherRoleNode,
            String otherFullName, String otherPicture,
            String lastMessage, String time, Object timestamp,
            int unreadMessages
    ) {
        this.id = id;
        this.otherId = otherId;
        this.otherRoleNode = otherRoleNode;
        this.otherFullName = otherFullName;
        this.otherPicture = otherPicture;
        this.lastMessage = lastMessage;
        this.time = time;
        this.timestamp = timestamp;
        this.unreadMessages = unreadMessages;
    }

    @Exclude
    public String getId() {
        return this.id;
    }

    @Exclude
    public void setId(String id) {
        this.id = id;
    }

    public String getOtherId() {
        return this.otherId;
    }

    public void setOtherId(String otherId) {
        this.otherId = otherId;
    }

    public String getOtherRoleNode() {
        return this.otherRoleNode;
    }

    public void setOtherRoleNode(String otherRoleNode) {
        this.otherRoleNode = otherRoleNode;
    }

    public String getOtherFullName() {
        return this.otherFullName;
    }

    public void setOtherFullName(String otherFullName) {
        this.otherFullName = otherFullName;
    }

    public String getOtherPicture() {
        return this.otherPicture;
    }

    public void setOtherPicture(String otherPicture) {
        this.otherPicture = otherPicture;
    }

    public String getLastMessage() {
        return this.lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getTime() {
        return this.time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Object getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(Object timestamp) {
        this.timestamp = timestamp;
    }

    public int getUnreadMessages() {
        return this.unreadMessages;
    }

    public void setUnreadMessages(int unreadMessages) {
        this.unreadMessages = unreadMessages;
    }
}
