package org.bernatpol.pocketmoodle.chat;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

class ChatsViewModelFactory implements ViewModelProvider.Factory {
    private String userRoleNode;

    ChatsViewModelFactory(String userRoleNode) {
        this.userRoleNode = userRoleNode;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new ChatsViewModel(this.userRoleNode);
    }
}
