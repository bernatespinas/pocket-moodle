package org.bernatpol.pocketmoodle.chat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.bernatpol.pocketmoodle.R;

import java.util.ArrayList;

class ChatsAdapter extends RecyclerView.Adapter<ChatsAdapter.ChatViewHolder> {
    private ArrayList<Chat> chats;
    private Context context;
    private OnChatClickListener onChatClickListener;

    ChatsAdapter(ArrayList<Chat> chats, Context context, OnChatClickListener onChatClickListener) {
        this.chats = chats;
        this.context = context;
        this.onChatClickListener = onChatClickListener;
    }

    @NonNull
    @Override
    public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.chat_item, parent, false)
        ;

        return new ChatViewHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatViewHolder holder, int position) {
        final Chat chat = this.chats.get(position);

        Glide.with(this.context)
                .load(chat.getOtherPicture())
                .apply(RequestOptions.circleCropTransform())
                .into(holder.otherPicture)
        ;

        holder.otherName.setText(chat.getOtherFullName());
        holder.lastMessage.setText(chat.getLastMessage());
        holder.time.setText(chat.getTime());

        if (chat.getUnreadMessages() > 0) {
            holder.unreadMessagesIndicator.setVisibility(View.VISIBLE);
        } else {
            holder.unreadMessagesIndicator.setVisibility(View.GONE);
        }

        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChatsAdapter.this.onChatClickListener.onChatClick(chat);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.chats.size();
    }

    static class ChatViewHolder extends RecyclerView.ViewHolder {
        private ConstraintLayout parent;

        private TextView otherName;
        private ImageView otherPicture;

        private TextView lastMessage;
        private TextView time;

        private ImageView unreadMessagesIndicator;

        ChatViewHolder(@NonNull View itemView) {
            super(itemView);

            this.parent = itemView.findViewById(R.id.parent);

            this.otherName = itemView.findViewById(R.id.other_name);
            this.otherPicture = itemView.findViewById(R.id.other_picture);

            this.lastMessage = itemView.findViewById(R.id.last_message);
            this.time = itemView.findViewById(R.id.time);

            this.unreadMessagesIndicator = itemView.findViewById(R.id.unread_messages_indicator);
        }
    }

    interface OnChatClickListener {
        void onChatClick(Chat chat);
    }
}
