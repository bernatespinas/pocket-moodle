package org.bernatpol.pocketmoodle.chat;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.bernatpol.pocketmoodle.R;
import org.bernatpol.pocketmoodle.user.User;
import org.bernatpol.pocketmoodle.user.UserPickerActivity;

import java.util.ArrayList;

public class ChatsFragment extends Fragment implements ChatsAdapter.OnChatClickListener, View.OnClickListener, ChatStudentsTeachersBottomSheetDialogFragment.OnUserRoleClickedListener {
    private static final int PICK_USER = 666;

    private ChatsViewModel chatsViewModel;
    private ArrayList<String> excludedUsersIds;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.getContext());
        String userRoleNode = sharedPreferences.getString("userRoleNode", "students");

        //Se crea ViewModel, asociado al Fragment. Cuando desaparezca el fragment, desaparece el ViewModel.
        this.chatsViewModel = new ViewModelProvider(
                this,
                new ChatsViewModelFactory(userRoleNode)
        ).get(ChatsViewModel.class);

        final View root = inflater.inflate(R.layout.fragment_chats, container, false);

        final RecyclerView chatList = root.findViewById(R.id.chat_list);
        chatList.addItemDecoration(
                new DividerItemDecoration(chatList.getContext(), DividerItemDecoration.VERTICAL)
        );

        this.chatsViewModel.getChats().observe(getViewLifecycleOwner(), new Observer<ArrayList<Chat>>() {
            @Override
            public void onChanged(ArrayList<Chat> chats) {
                TextView chatsStatus = root.findViewById(R.id.status);

                if (chats.isEmpty()) {
                    chatsStatus.setText(R.string.nothing_here);
                } else {

                    chatList.setLayoutManager(
                            new LinearLayoutManager(ChatsFragment.this.getContext())
                    );
                    chatList.setAdapter(
                            new ChatsAdapter(chats, ChatsFragment.this.getContext(), ChatsFragment.this)
                    );

                    chatsStatus.setVisibility(View.GONE);
                    chatList.setVisibility(View.VISIBLE);
                }

                FloatingActionButton fab = root.findViewById(R.id.new_chat);
                fab.setVisibility(View.VISIBLE);

                // Users with already started chats will be excluded.
                ChatsFragment.this.excludedUsersIds = new ArrayList<>();
                for (Chat chat : chats) {
                    ChatsFragment.this.excludedUsersIds.add(chat.getOtherId());
                }

                fab.setOnClickListener(ChatsFragment.this);
            }
        });

        return root;
    }

    @Override
    public void onChatClick(Chat chat) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("chat", chat);

        NavHostFragment
                .findNavController(this)
                .navigate(R.id.action_nav_chats_to_nav_chat_messages, bundle)
        ;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ChatsFragment.PICK_USER && resultCode == Activity.RESULT_OK) {
            User otherUser = (User) data.getSerializableExtra("user");

            Toast.makeText(this.getContext(), "Clicked user " + otherUser.getFullName(), Toast.LENGTH_SHORT).show();

            Bundle bundle = new Bundle();
            bundle.putSerializable("chat", this.chatsViewModel.createChat(otherUser));

            NavHostFragment
                    .findNavController(this)
                    .navigate(R.id.action_nav_chats_to_nav_chat_messages, bundle)
            ;
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.new_chat) {
            new ChatStudentsTeachersBottomSheetDialogFragment(this)
                    .show(getChildFragmentManager(), "new_chat")
            ;
        }
    }

    @Override
    public void onUserRoleClicked(boolean students) {
        Intent intent = new Intent(ChatsFragment.this.getContext(), UserPickerActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean("students", students);
        bundle.putStringArrayList("excludedUsersIds", this.excludedUsersIds);
        intent.putExtras(bundle);
        startActivityForResult(intent, ChatsFragment.PICK_USER);
    }
}
