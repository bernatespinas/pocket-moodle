package org.bernatpol.pocketmoodle.subject;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class SubjectsViewModelFactory implements ViewModelProvider.Factory {
    private boolean student;

    public SubjectsViewModelFactory(boolean student) {
        this.student = student;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new SubjectsViewModel(this.student);
    }
}
