package org.bernatpol.pocketmoodle.subject.subjects_list;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.collection.LLRBNode;

import org.bernatpol.pocketmoodle.R;
import org.bernatpol.pocketmoodle.subject.Subject;

import java.util.ArrayList;

class SubjectsListAdapter extends RecyclerView.Adapter<SubjectsListAdapter.SubjectsListViewHolder> {
    private ArrayList<Subject> subjects;
    private OnSubjectClickListener onSubjectClickListener;

    SubjectsListAdapter(ArrayList<Subject> subjects, OnSubjectClickListener onSubjectClickListener) {
        this.subjects = subjects;
        this.onSubjectClickListener = onSubjectClickListener;
    }

    @NonNull
    @Override
    public SubjectsListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.subject_topic_item, parent, false);
        return new SubjectsListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SubjectsListViewHolder holder, int position) {
        final Subject subject = this.subjects.get(position);

        holder.id.setText(subject.getId().toUpperCase());
        holder.name.setText(subject.getName());

        holder.id.setBackground(ContextCompat.getDrawable(
                this.onSubjectClickListener.getListenerContext(),
                R.drawable.selected_role_background
        ));
        holder.id.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(subject.getColor())));

        if (!subject.isAvailableForCurrentUser()) {
            holder.status.setImageResource(R.drawable.ic_lock_24dp);
            holder.status.setVisibility(View.VISIBLE);
        } else if (!subject.isVisible()) {
            holder.status.setImageResource(R.drawable.ic_visibility_off_24dp);
            holder.status.setVisibility(View.VISIBLE);
        } else {
            holder.status.setVisibility(View.GONE);
        }

        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubjectsListAdapter.this.onSubjectClickListener.onSubjectClick(subject);
            }
        });
        holder.parent.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return SubjectsListAdapter.this.onSubjectClickListener.onSubjectLongClick(subject);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.subjects.size();
    }

    static class SubjectsListViewHolder extends RecyclerView.ViewHolder {
        private ConstraintLayout parent;

        private TextView id;
        private TextView name;
        private ImageView status;

        SubjectsListViewHolder(@NonNull View itemView) {
            super(itemView);

            this.parent = itemView.findViewById(R.id.parent);

            this.id = itemView.findViewById(R.id.id);
            this.name = itemView.findViewById(R.id.name);
            this.status = itemView.findViewById(R.id.status);
        }
    }

    interface OnSubjectClickListener {
        void onSubjectClick(Subject subject);
        boolean onSubjectLongClick(Subject subject);
        Context getListenerContext();
    }
}
