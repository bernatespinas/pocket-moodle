package org.bernatpol.pocketmoodle.subject.subject_details;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.bernatpol.pocketmoodle.R;
import org.bernatpol.pocketmoodle.subject.Subject;
import org.bernatpol.pocketmoodle.subject.SubjectsViewModel;
import org.bernatpol.pocketmoodle.subject.VisibilityBottomSheetDialogFragment;
import org.bernatpol.pocketmoodle.topic.Topic;
import org.bernatpol.pocketmoodle.topic.TopicAdapter;
import org.bernatpol.pocketmoodle.user.UserPickerActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class SubjectDetailsFragment extends Fragment implements View.OnClickListener, TopicAdapter.OnTopicClickListener {
    private String subjectId;
    private String color;
    private boolean student;

    public SubjectDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_subject_details, container, false);

        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.getContext());
        this.student = sharedPreferences.getBoolean("student", true);

        Bundle bundle = getArguments();
        this.subjectId = bundle.getString("subjectId").toLowerCase();

        // Get the already available SubjectsViewModel created in MainActivity.
        SubjectsViewModel subjectsViewModel = new ViewModelProvider(
                requireActivity()
        ).get(SubjectsViewModel.class);
        // Fetch the single Subject.
        subjectsViewModel.fetchSingleSubject(subjectId);

        final RecyclerView topicList = root.findViewById(R.id.topic_list);
        topicList.addItemDecoration(
                new DividerItemDecoration(topicList.getContext(), DividerItemDecoration.VERTICAL)
        );

        subjectsViewModel.getSingleSubject().observe(getViewLifecycleOwner(), new Observer<Subject>() {
            @Override
            public void onChanged(Subject subject) {
                SubjectDetailsFragment.this.color = subject.getColor();

                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(subject.getIdAndName());
                ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor(subject.getColor())));

                topicList.setLayoutManager(new LinearLayoutManager(
                        SubjectDetailsFragment.this.getContext()
                ));

                topicList.setAdapter(new TopicAdapter(
                        subject.getTopics(),
                        SubjectDetailsFragment.this
                ));

                root.findViewById(R.id.status).setVisibility(View.GONE);
                topicList.setVisibility(View.VISIBLE);
            }
        });

        SubjectDetailsFragment.this.fillStudentsTeachersCardButton(root);
        SubjectDetailsFragment.this.fillQualificationsCardButton(root);

        return root;
    }

    //Sirve para no mantener el color de la cabezera, al volver para atrás.
    @Override
    public void onDetach() {
        super.onDetach();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
    }

    private void fillStudentsTeachersCardButton(View root) {
        CardView students = root.findViewById(R.id.students);

        ((TextView) students.findViewById(R.id.title))
                .setText(R.string.students)
        ;
        ((ImageView) students.findViewById(R.id.picture))
                .setImageResource(R.drawable.ic_people_24dp)
        ;

        students.setOnClickListener(this);

        CardView teachers = root.findViewById(R.id.teachers);

        ((TextView) teachers.findViewById(R.id.title))
                .setText(R.string.teachers)
        ;
        ((ImageView) teachers.findViewById(R.id.picture))
                .setImageResource(R.drawable.ic_school_24dp)
        ;

        teachers.setOnClickListener(this);
    }

    private void fillQualificationsCardButton(View root) {
        CardView qualifications = root.findViewById(R.id.qualifications);

        ((ImageView) qualifications.findViewById(R.id.picture))
                .setImageResource(R.drawable.ic_grid_on_24dp)
        ;
        ((TextView) qualifications.findViewById(R.id.title))
                .setText(R.string.qualifications)
        ;

        qualifications.setOnClickListener(this);
    }

    @Override
    public void onTopicClick(Topic topic) {
        if (!topic.isAvailableForCurrentUser()) {
            Toast.makeText(
                    this.getContext(),
                    "You are not enrolled in this topic",
                    Toast.LENGTH_SHORT
            ).show();

        } else if (!topic.isVisible() && this.student) {
            // Only students cannot access their non-visible Topics.
            Toast.makeText(
                    this.getContext(),
                    "This topic is not visible",
                    Toast.LENGTH_SHORT
            ).show();

        } else {
            Bundle bundle = new Bundle();
            bundle.putString("topicId", topic.getId().toLowerCase());
            bundle.putString("subjectId", this.subjectId.toLowerCase());
            bundle.putString("color", this.color);

            NavHostFragment.findNavController(this)
                    .navigate(R.id.action_nav_subjects_details_to_topic, bundle)
            ;
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.students) {
            Intent intent = new Intent(this.getContext(), UserPickerActivity.class);
            Bundle bundle = new Bundle();
            bundle.putBoolean("students", true);
            bundle.putString("subjectId", this.subjectId.toLowerCase());
            intent.putExtras(bundle);
            startActivity(intent);

        } else if (id == R.id.teachers) {
            Intent intent = new Intent(this.getContext(), UserPickerActivity.class);
            Bundle bundle = new Bundle();
            bundle.putBoolean("students", false);
            bundle.putString("subjectId", this.subjectId.toLowerCase());
            intent.putExtras(bundle);
            startActivity(intent);

        } else if (id == R.id.qualifications) {
            Toast.makeText(
                    SubjectDetailsFragment.this.getContext(),
                    "has suspès",
                    Toast.LENGTH_SHORT
            ).show();
        }
    }

    @Override
    public boolean onTopicLongClick(Topic topic) {
        if (this.student) {
            return false;
        } else {
            new VisibilityBottomSheetDialogFragment(this.subjectId, topic.getId()).show(getChildFragmentManager(), "Topic Master");
            return true;
        }
    }
}
