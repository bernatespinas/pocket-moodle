package org.bernatpol.pocketmoodle.subject;

import com.google.firebase.database.Exclude;

import org.bernatpol.pocketmoodle.topic.Topic;

import java.io.Serializable;
import java.util.ArrayList;

public class Subject implements Serializable {
    private String id;
    private String name;
    private boolean visible;
    private String currentTopic;
    private String color;

    private ArrayList<Topic> topics;

    /**
     * This value is obtained by checking if the current user's UID figures as a teacher for
     * this Subject or as a student enrolled in any of its Topics. In that case it will be true.
     */
    private boolean availableForCurrentUser;

    @Exclude
    public String getId() {
        return this.id;
    }

    @Exclude
    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isVisible() {
        return this.visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getCurrentTopic() {
        return this.currentTopic;
    }

    public void setCurrentTopic(String currentTopic) {
        this.currentTopic = currentTopic;
    }

    public String getColor() {
        return this.color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Exclude
    public ArrayList<Topic> getTopics() {
        return this.topics;
    }

    @Exclude
    public void setTopics(ArrayList<Topic> topics) {
        this.topics = topics;
    }

    @Exclude
    public boolean isAvailableForCurrentUser() {
        return this.availableForCurrentUser;
    }

    @Exclude
    public void setAvailableForCurrentUser(boolean availableForCurrentUser) {
        this.availableForCurrentUser = availableForCurrentUser;
    }

    @Exclude
    public boolean isVisibleAndAvailableForCurrentUser() {
        return this.isVisible() && this.isAvailableForCurrentUser();
    }

    @Exclude
    public String getIdAndName() {
        return this.id.toUpperCase() + " - " + this.name;
    }
}
