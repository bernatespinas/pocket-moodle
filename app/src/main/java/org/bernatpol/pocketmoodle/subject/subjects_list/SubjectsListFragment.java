package org.bernatpol.pocketmoodle.subject.subjects_list;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.bernatpol.pocketmoodle.R;
import org.bernatpol.pocketmoodle.subject.Subject;
import org.bernatpol.pocketmoodle.subject.SubjectsViewModel;
import org.bernatpol.pocketmoodle.subject.VisibilityBottomSheetDialogFragment;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class SubjectsListFragment extends Fragment implements SubjectsListAdapter.OnSubjectClickListener {
    private boolean student;

    public SubjectsListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.getContext());
        this.student = sharedPreferences.getBoolean("student", true);

        // Get the already available SubjectsViewModel created in MainActivity.
        SubjectsViewModel subjectsViewModel = new ViewModelProvider(
                requireActivity()
        ).get(SubjectsViewModel.class);

        final View root = inflater.inflate(R.layout.fragment_subjects_list, container, false);

        // .addItemDecoration should be called only once, because otherwise it starts to stack or something -_-.
        final RecyclerView subjectList = root.findViewById(R.id.subject_list);
        subjectList.addItemDecoration(
                new DividerItemDecoration(subjectList.getContext(), DividerItemDecoration.VERTICAL)
        );

        subjectsViewModel.getSubjects().observe(getViewLifecycleOwner(), new Observer<HashMap<String, Subject>>() {
            @Override
            public void onChanged(HashMap<String, Subject> subjects) {
                subjectList.setLayoutManager(
                        new LinearLayoutManager(SubjectsListFragment.this.getContext())
                );

                subjectList.setAdapter(
                        new SubjectsListAdapter(new ArrayList<>(subjects.values()), SubjectsListFragment.this)
                );

                root.findViewById(R.id.status).setVisibility(View.GONE);
                subjectList.setVisibility(View.VISIBLE);
            }
        });

        return root;
    }

    @Override
    public void onSubjectClick(Subject subject) {
        if (!subject.isAvailableForCurrentUser()) {
            Toast.makeText(
                    this.getContext(),
                    "You are not enrolled in this subject",
                    Toast.LENGTH_SHORT
            ).show();

        } else if (!subject.isVisible() && this.student) {
            // Only Students cannot access their non-visible Subjects.
            Toast.makeText(
                    this.getContext(),
                    "This subject is not visible",
                    Toast.LENGTH_SHORT
            ).show();
            
        } else {
            Bundle bundle = new Bundle();
//            bundle.putSerializable("subject", subject);
            // Even though we already have the Subject object, we pass its ID in order
            // to fetch its Firebase node again and listen for changes there.
            bundle.putString("subjectId", subject.getId());

            NavHostFragment.findNavController(this)
                    .navigate(R.id.action_nav_subjects_list_to_subject_details, bundle)
            ;

            ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor(subject.getColor())));
        }
    }

    @Override
    public boolean onSubjectLongClick(Subject subject) {
        if (this.student || !subject.isAvailableForCurrentUser()) {
            return false;
        } else {
            new VisibilityBottomSheetDialogFragment(subject.getId()).show(getChildFragmentManager(), "Subject lol");
            return true;
        }
    }

    @Override
    public Context getListenerContext() {
        return this.getContext();
    }
}
