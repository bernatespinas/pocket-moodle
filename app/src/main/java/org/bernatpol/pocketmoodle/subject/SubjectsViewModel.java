package org.bernatpol.pocketmoodle.subject;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.bernatpol.pocketmoodle.topic.Topic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

public class SubjectsViewModel extends ViewModel {
    // An HashMap that contains every Subject, available all of the time.
    // Keys are Subjects' IDs so they can be obtained by key easily.
    private MutableLiveData<HashMap<String, Subject>> subjects;
    // A single Subject, to avoid listening for changes in other unrelated Subjects.
    private MutableLiveData<Subject> singleSubject;

    private boolean student;

    SubjectsViewModel(final boolean student) {
        // This value is obtained in TimetableFragment via .getValue(), with no Observer,
        // so it should have a default value in case it's not ready when to avoid an Exception.
//        this.subjects = new MutableLiveData<>(new HashMap<String, Subject>());
        // Not anymore!
        this.subjects = new MutableLiveData<>();

        this.student = student;

        FirebaseDatabase.getInstance()
                .getReference("2wiam/subjects")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        HashMap<String, Subject> subjects = new HashMap<>();

                        for (DataSnapshot subjectSnapshot : dataSnapshot.getChildren()) {
                            Log.d("SUBJECT", subjectSnapshot.getKey());
                            Subject subject = subjectSnapshot.getValue(Subject.class);
                            subject.setId(subjectSnapshot.getKey());

                            if (SubjectsViewModel.this.student) {
                                SubjectsViewModel.this.setStudentSubjectAndTopicsAvailability(subject, subjectSnapshot);
                            } else {
                                SubjectsViewModel.this.setTeacherSubjectAvailability(subject, subjectSnapshot);
                            }

                            subjects.put(subject.getId(), subject);
                        }

                        SubjectsViewModel.this.subjects.setValue(subjects);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                })
        ;
    }

    public void fetchSingleSubject(String subjectId) {
        this.singleSubject = new MutableLiveData<>();

        FirebaseDatabase.getInstance()
                .getReference("2wiam/subjects/" + subjectId.toLowerCase())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot subjectSnapshot) {
                        Log.d("SUBJECT", subjectSnapshot.getKey());
                        Subject subject = subjectSnapshot.getValue(Subject.class);
                        subject.setId(subjectSnapshot.getKey());

                        if (SubjectsViewModel.this.student) {
                            SubjectsViewModel.this.setStudentSubjectAndTopicsAvailability(subject, subjectSnapshot);
                        } else {
                            SubjectsViewModel.this.setTeacherSubjectAvailability(subject, subjectSnapshot);
                        }

                        SubjectsViewModel.this.singleSubject.setValue(subject);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                })
        ;
    }

    private void setStudentSubjectAndTopicsAvailability(Subject subject, DataSnapshot subjectSnapshot) {
        String userUid = FirebaseAuth.getInstance().getCurrentUser().getUid();

        boolean subjectAvailableForCurrentUser = false;
        ArrayList<Topic> topics = new ArrayList<>();

        for (DataSnapshot topicSnapshot : subjectSnapshot.child("topics").getChildren()) {
            Topic topic = topicSnapshot.getValue(Topic.class);

            boolean topicAvailableForCurrentUser = topicSnapshot.child("students").child(userUid).exists();
            topic.setAvailableForCurrentUser(topicAvailableForCurrentUser);

            if (topicAvailableForCurrentUser) {
                subjectAvailableForCurrentUser = true;
            }

            topics.add(topic);
        }

        subject.setTopics(topics);
        subject.setAvailableForCurrentUser(subjectAvailableForCurrentUser);
    }

    private void setTeacherSubjectAvailability(Subject subject, DataSnapshot subjectSnapshot) {
        String userUid = FirebaseAuth.getInstance().getCurrentUser().getUid();

        boolean subjectAvailableForCurrentUser = subjectSnapshot.child("teachers").child(userUid).exists();

        ArrayList<Topic> topics = new ArrayList<>();

        for (DataSnapshot topicSnapshot : subjectSnapshot.child("topics").getChildren()) {
            Topic topic = topicSnapshot.getValue(Topic.class);

            topic.setAvailableForCurrentUser(subjectAvailableForCurrentUser);

            topics.add(topic);
        }

        subject.setTopics(topics);
        subject.setAvailableForCurrentUser(subjectAvailableForCurrentUser);
    }

    static void toggleSubjectVisibility(String subjectId) {
        SubjectsViewModel.toggleItemVisibility("2wiam/subjects/" + subjectId.toLowerCase());
    }

    static void toggleTopicVisibility(String subjectId, String topicId) {
        SubjectsViewModel.toggleItemVisibility(String.format("2wiam/subjects/%s/topics/%s", subjectId, topicId.toLowerCase()));

    }

    static void toggleTopicItemVisibility(String subjectId, String topicId, String itemTypeAndId) {
        SubjectsViewModel.toggleItemVisibility(String.format("2wiam/topicItems/%s/%s/%s", subjectId, topicId, itemTypeAndId));
    }

    private static void toggleItemVisibility(String path) {
        final DatabaseReference reference = FirebaseDatabase.getInstance()
                .getReference(path)
        ;

        reference
                .addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    reference.child("visible").setValue(
                            !dataSnapshot.child("visible").getValue(Boolean.class)
                    );
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
        });
    }

    public LiveData<HashMap<String, Subject>> getSubjects() {
        return this.subjects;
    }

    public LiveData<Subject> getSingleSubject() {
        return this.singleSubject;
    }
}
