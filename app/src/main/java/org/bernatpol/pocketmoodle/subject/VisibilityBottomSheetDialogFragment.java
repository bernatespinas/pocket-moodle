package org.bernatpol.pocketmoodle.subject;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.bernatpol.pocketmoodle.R;

import java.util.Optional;

public class VisibilityBottomSheetDialogFragment extends BottomSheetDialogFragment implements View.OnClickListener {
    private String subjectId;
    private Optional<String> topicId;
    private Optional<String> itemTypeAndId;

    public VisibilityBottomSheetDialogFragment(String subjectId) {
        this.subjectId = subjectId;
        this.topicId = Optional.empty();
        this.itemTypeAndId = Optional.empty();
    }

    public VisibilityBottomSheetDialogFragment(String subjectId, String topicId) {
        this.subjectId = subjectId;
        this.topicId = Optional.of(topicId);
        this.itemTypeAndId = Optional.empty();
    }

    public VisibilityBottomSheetDialogFragment(String subjectId, String topicId, String itemTypeAndId) {
        this.subjectId = subjectId;
        this.topicId = Optional.of(topicId);
        this.itemTypeAndId = Optional.of(itemTypeAndId);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.subject_topic_visibility_dialog, container, false);

        root.findViewById(R.id.toggle_visibility).setOnClickListener(this);

        return root;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.toggle_visibility) {
            if (this.topicId.isPresent()) {
                if (this.itemTypeAndId.isPresent()) {
                    SubjectsViewModel.toggleTopicItemVisibility(this.subjectId, this.topicId.get(), this.itemTypeAndId.get());
                } else {
                    SubjectsViewModel.toggleTopicVisibility(this.subjectId, this.topicId.get());
                }
            } else {
                SubjectsViewModel.toggleSubjectVisibility(this.subjectId);
            }
            this.dismiss();
        }
    }
}
