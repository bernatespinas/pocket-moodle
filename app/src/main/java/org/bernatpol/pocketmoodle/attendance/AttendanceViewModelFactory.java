package org.bernatpol.pocketmoodle.attendance;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class AttendanceViewModelFactory implements ViewModelProvider.Factory {
    private String path;

    AttendanceViewModelFactory(String path) {
        this.path = path;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new AttendanceViewModel(this.path);
    }
}
