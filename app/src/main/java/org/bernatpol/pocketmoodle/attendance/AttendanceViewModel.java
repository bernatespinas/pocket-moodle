package org.bernatpol.pocketmoodle.attendance;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

class AttendanceViewModel extends ViewModel {
    private MutableLiveData<HashMap<String, String>> studentsAttendance;

    AttendanceViewModel(String path) {
        this.studentsAttendance = new MutableLiveData<>();

        FirebaseDatabase.getInstance()
                .getReference(path)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot attendanceSnapshot) {
                        HashMap<String, String> _studentsAttendance = new HashMap<>();

                        if (attendanceSnapshot.exists()) {
                            for (DataSnapshot studentIdSnapshot : attendanceSnapshot.getChildren()) {
                                _studentsAttendance.put(
                                        studentIdSnapshot.getKey(),
                                        studentIdSnapshot.getValue(String.class)
                                );
                            }
                        }

                        AttendanceViewModel.this.studentsAttendance.setValue(_studentsAttendance);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                })
        ;
    }

    static void saveAttendance(HashMap<String, String> studentsAttendance, String path) {
        FirebaseDatabase.getInstance().getReference(path).setValue(studentsAttendance);
    }

    LiveData<HashMap<String, String>> getStudentsAttendance() {
        return this.studentsAttendance;
    }
}
