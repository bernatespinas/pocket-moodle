package org.bernatpol.pocketmoodle.attendance;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.bernatpol.pocketmoodle.R;
import org.bernatpol.pocketmoodle.subject.Subject;
import org.bernatpol.pocketmoodle.subject.SubjectsViewModel;
import org.bernatpol.pocketmoodle.user.User;
import org.bernatpol.pocketmoodle.user.UsersViewModel;
import org.bernatpol.pocketmoodle.user.UsersViewModelFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class AttendanceFragment extends Fragment implements View.OnClickListener {
    private AttendanceViewModel attendanceViewModel;
    private AttendanceAdapter attendanceAdapter;

    private String path;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_attendance, container, false);

        Bundle bundle = this.getArguments();
        this.path = bundle.getString("path");
        final String subjectId = bundle.getString("subjectId");

        if (subjectId == null) {
            // Attendance has already been taken. Fetch students' names and pictures.
            this.attendanceViewModel = new ViewModelProvider(
                    this,
                    new AttendanceViewModelFactory(this.path)
            ).get(AttendanceViewModel.class);

            this.attendanceViewModel.getStudentsAttendance().observe(getViewLifecycleOwner(), new Observer<HashMap<String, String>>() {
                @Override
                public void onChanged(final HashMap<String, String> stringStringHashMap) {
                    UsersViewModel usersViewModel = new ViewModelProvider(
                            AttendanceFragment.this,
                            new UsersViewModelFactory(true, new ArrayList<>(stringStringHashMap.keySet()))
                    ).get(UsersViewModel.class);

                    usersViewModel.getPeople().observe(getViewLifecycleOwner(), new Observer<ArrayList<User>>() {
                        @Override
                        public void onChanged(ArrayList<User> users) {
                            AttendanceFragment.this.populateUsersList(users, stringStringHashMap, root);
                        }
                    });
                }
            });

        } else {
            // Attendance hasn't been taken yet. Fetch required students and their names and pictures.
            SubjectsViewModel subjectsViewModel = new ViewModelProvider(
                    requireActivity()
            ).get(SubjectsViewModel.class);

            subjectsViewModel.getSubjects().observe(getViewLifecycleOwner(), new Observer<HashMap<String, Subject>>() {
                @Override
                public void onChanged(HashMap<String, Subject> stringSubjectHashMap) {
                    String currentTopicId = stringSubjectHashMap.get(subjectId).getCurrentTopic();

                    UsersViewModel usersViewModel = new ViewModelProvider(
                            AttendanceFragment.this,
                            new UsersViewModelFactory(subjectId, currentTopicId, new HashSet<String>())
                    ).get(UsersViewModel.class);

                    usersViewModel.getPeople().observe(getViewLifecycleOwner(), new Observer<ArrayList<User>>() {
                        @Override
                        public void onChanged(ArrayList<User> users) {
                            AttendanceFragment.this.populateUsersList(users, new HashMap<String, String>(), root);
                        }
                    });
                }
            });
        }

        return root;
    }

    private void populateUsersList(ArrayList<User> students, HashMap<String, String> studentsAttendance, View root) {
        RecyclerView studentList = root.findViewById(R.id.student_list);

        studentList.setLayoutManager(
                new LinearLayoutManager(AttendanceFragment.this.getContext())
        );

        this.attendanceAdapter = new AttendanceAdapter(students, studentsAttendance, AttendanceFragment.this.getContext());
        studentList.setAdapter(this.attendanceAdapter);

        studentList.addItemDecoration(
                new DividerItemDecoration(AttendanceFragment.this.getContext(), DividerItemDecoration.VERTICAL)
        );

        root.findViewById(R.id.status).setVisibility(View.GONE);
        studentList.setVisibility(View.VISIBLE);

        Button save = root.findViewById(R.id.save);
        save.setOnClickListener(this);
        save.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.save) {
            Toast.makeText(this.getContext(), "Save button clicked", Toast.LENGTH_SHORT).show();
            AttendanceViewModel.saveAttendance(this.attendanceAdapter.getStudentsAttendance(), this.path);
        }
    }
}
