package org.bernatpol.pocketmoodle.attendance;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.bernatpol.pocketmoodle.R;
import org.bernatpol.pocketmoodle.user.User;

import java.util.ArrayList;
import java.util.HashMap;

class AttendanceAdapter extends RecyclerView.Adapter<AttendanceAdapter.AttendanceViewHolder> {
    private static final HashMap<String, Integer> ATTENDANCE_POSITION = new HashMap<>();
    static {
        ATTENDANCE_POSITION.put("Present", 0);
        ATTENDANCE_POSITION.put("Absent", 1);
        ATTENDANCE_POSITION.put("Justified", 2);
        ATTENDANCE_POSITION.put("Late", 3);
    }

    private ArrayList<User> students;
    private HashMap<String, String> studentsAttendance;

    private Context context;

    AttendanceAdapter(ArrayList<User> students, HashMap<String, String> _studentsAttendance, Context context) {
        this.students = students;

        if (_studentsAttendance.isEmpty()) {
            this.studentsAttendance = new HashMap<>();

            for (User student : students) {
                if (_studentsAttendance.containsKey(student.getId())) {
                    this.studentsAttendance.put(student.getId(), _studentsAttendance.get(student.getId()));
                } else {
                    this.studentsAttendance.put(student.getId(), "Present");
                }
            }
        } else {
            this.studentsAttendance = _studentsAttendance;
        }

//        this.studentsAttendance = studentsAttendance;

        this.context = context;
    }

    @NonNull
    @Override
    public AttendanceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.attendance_item, parent, false)
        ;

        return new AttendanceViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final AttendanceViewHolder holder, int position) {
        final User student = this.students.get(position);

        Glide.with(this.context)
                .load(student.getPicture())
                .apply(RequestOptions.circleCropTransform())
                .into(holder.picture)
        ;

        holder.fullName.setText(student.getFullName());
        holder.attendance.setSelection(ATTENDANCE_POSITION.get(this.studentsAttendance.get(student.getId())));
        // Keep the studentsAttendance HashMap updated. When the user selects a value from a Spinner,
        // store it.
        holder.attendance.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                AttendanceAdapter.this.studentsAttendance.put(student.getId(), (String) holder.attendance.getSelectedItem());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.students.size();
    }

    HashMap<String, String> getStudentsAttendance() {
        return this.studentsAttendance;
    }

    static class AttendanceViewHolder extends RecyclerView.ViewHolder {
        private ImageView picture;
        private TextView fullName;
        private Spinner attendance;

        AttendanceViewHolder(@NonNull View itemView) {
            super(itemView);

            this.picture = itemView.findViewById(R.id.picture);
            this.fullName = itemView.findViewById(R.id.full_name);
            this.attendance = itemView.findViewById(R.id.attendance);
        }
    }
}
